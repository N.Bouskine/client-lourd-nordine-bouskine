import java.sql.SQLException;
import java.sql.Connection;

/**
 * @author 33662
 *
 */
public class App {
	
	public static void main(String[] args) throws ClassNotFoundException {
		
		//Controller c = new Controller();
		//View v = new View();
		//v.setVisible(true);
		//s.connect();
		
		try {
			DAOLaboratoire daop = new DAOLaboratoire(Singleton.getInstance().con);
			DAOMedecin daom = new DAOMedecin(Singleton.getInstance().con);
			DAOCommercial daoc = new DAOCommercial(Singleton.getInstance().con);
			DAOMedicament daod = new DAOMedicament(Singleton.getInstance().con);
			DAORencontre daor = new DAORencontre(Singleton.getInstance().con);
			DAOUtilisateur daou = new DAOUtilisateur(Singleton.getInstance().con);
			DAOConnection daocx = new DAOConnection(Singleton.getInstance().con);
			View v = new View();
			Connexion co = new Connexion();
			Controller c = new Controller(co,v,daop,daom,daoc,daod,daocx,daor,daou);
			co.setVisible(true);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
	}

}

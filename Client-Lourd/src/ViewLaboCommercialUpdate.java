
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Window.Type;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpringLayout;

public class ViewLaboCommercialUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JLabel lblNewLabel_1;
	private JTextField textField_3;
	private JTextField textField_4;
	public JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewLaboratoireUpdate frame = new ViewLaboratoireUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewLaboCommercialUpdate() {
		setBounds(100, 100, 482, 619);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.BLACK);
		
		JLabel lblNewLabel = new JLabel("Modifier les informations de l'employ\u00E9e");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setForeground(Color.WHITE);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JLabel lblNewLabel_2 = new JLabel("Identifiant de l'employ\u00E9e");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_3 = new JLabel("Nom de l'employ\u00E9e");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_4 = new JLabel("Pr\u00E9nom de l'employ\u00E9e");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_5 = new JLabel("Mail de l'employ\u00E9e");
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		textField = new JTextField();
		textField.setBackground(Color.WHITE);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		
		btnNewButton = new JButton("Modifier");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		btnNewButton_1 = new JButton("Annuler");
		btnNewButton_1.setBackground(Color.RED);
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		SpringLayout springLayout = new SpringLayout();
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, -59, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 0, SpringLayout.WEST, textField_2);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 317, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, -6, SpringLayout.WEST, btnNewButton_1);
		springLayout.putConstraint(SpringLayout.WEST, textField_2, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton_1, 418, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, textField_1, 210, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_1, -31, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_2, -31, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 421, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -59, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_3, 19, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_3, -18, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_4, 19, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_5, 39, SpringLayout.SOUTH, lblNewLabel_4);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_5, 0, SpringLayout.WEST, lblNewLabel_4);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_5, 192, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, textField_2, 0, SpringLayout.NORTH, lblNewLabel_4);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_4, 41, SpringLayout.SOUTH, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.NORTH, textField_1, 0, SpringLayout.NORTH, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_3, 37, SpringLayout.SOUTH, lblNewLabel_2);
		springLayout.putConstraint(SpringLayout.NORTH, textField, 38, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, textField, 210, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField, 427, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 41, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_2, 19, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_2, 192, SpringLayout.WEST, panel_1);
		panel_1.setLayout(springLayout);
		panel_1.add(btnNewButton);
		panel_1.add(btnNewButton_1);
		panel_1.add(lblNewLabel_5);
		panel_1.add(lblNewLabel_4);
		panel_1.add(lblNewLabel_3);
		panel_1.add(lblNewLabel_2);
		panel_1.add(textField_2);
		panel_1.add(textField_1);
		panel_1.add(textField);
		
		textField_3 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_3, 0, SpringLayout.NORTH, lblNewLabel_5);
		springLayout.putConstraint(SpringLayout.WEST, textField_3, 18, SpringLayout.EAST, lblNewLabel_5);
		springLayout.putConstraint(SpringLayout.EAST, textField_3, -31, SpringLayout.EAST, panel_1);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("T\u00E9l\u00E9phone de l'employ\u00E9e");
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setBackground(Color.BLACK);
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 13));
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_6, 44, SpringLayout.SOUTH, lblNewLabel_5);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_6, 0, SpringLayout.WEST, lblNewLabel_5);
		panel_1.add(lblNewLabel_6);
		
		JLabel lblNewLabel_6_1 = new JLabel("Laboratoire de l'employ\u00E9e");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_6_1, 34, SpringLayout.SOUTH, lblNewLabel_6);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_6_1, 0, SpringLayout.WEST, lblNewLabel_5);
		lblNewLabel_6_1.setForeground(Color.WHITE);
		lblNewLabel_6_1.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblNewLabel_6_1.setBackground(Color.BLACK);
		panel_1.add(lblNewLabel_6_1);
		
		textField_4 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_1, 139, SpringLayout.SOUTH, textField_4);
		springLayout.putConstraint(SpringLayout.NORTH, textField_4, 38, SpringLayout.SOUTH, textField_3);
		springLayout.putConstraint(SpringLayout.WEST, textField_4, 0, SpringLayout.WEST, textField_2);
		springLayout.putConstraint(SpringLayout.EAST, textField_4, -31, SpringLayout.EAST, panel_1);
		panel_1.add(textField_4);
		textField_4.setColumns(10);
		
		comboBox = new JComboBox();
		springLayout.putConstraint(SpringLayout.NORTH, comboBox, 34, SpringLayout.SOUTH, textField_4);
		springLayout.putConstraint(SpringLayout.WEST, comboBox, 21, SpringLayout.EAST, lblNewLabel_6_1);
		springLayout.putConstraint(SpringLayout.EAST, comboBox, 0, SpringLayout.EAST, textField_2);
		panel_1.add(comboBox);
		contentPane.setLayout(gl_contentPane);
	}
	public JTextField getTextField() {
		return textField;
	}
	public JTextField getTextField_1() {
		return textField_1;
	}
	public JTextField getTextField_2() {
		return textField_2;
	}
	
	public JButton getBtnNewButton() {
		return btnNewButton;
	}
	public JButton getBtnNewButton_1() {
		return btnNewButton_1;
	}
	
	public JLabel getLblNewLabel_1() {
		return lblNewLabel_1;
	}
	
	
	
	public void Afficher2(ViewCommercial v, View v1) throws  SQLException {
			
			
			
			lblNewLabel_1.setText(v.getLblNewLabel_1().getText().toString());
			
			textField.setText(v.getLblNewLabel_1().getText().toString());
			
			textField_1.setText(v.getTextArea().getText().toString());
			
			textField_2.setText(v.getTextArea_1().getText().toString());
			
			textField_3.setText(v.getTextArea_2().getText().toString());
			
			textField_4.setText(v.getTextArea_3().getText().toString());
			
			comboBox.setModel(v1.getComboBox_1().getModel());
			
			comboBox.setSelectedItem(v.getTextArea_4().getText());
			
			
			
			
			
		}
	
	public JTextField getTextField_3() {
		return textField_3;
	}
	public JTextField getTextField_4() {
		return textField_4;
	}
	
	public JComboBox getComboBox() {
		return comboBox;
	}
}

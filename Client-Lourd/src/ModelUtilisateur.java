import java.util.HashSet;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import Entities.Utilisateur;

public class ModelUtilisateur extends DefaultTableModel{
	
	List<Utilisateur> liste;
	String[] colNames = {"Identfiant","Role"};
	HashSet<Utilisateur> modified = new HashSet<>();
	
	
	public HashSet<Utilisateur> getModified() {
		return modified;
	}

	public void setModified(HashSet<Utilisateur> modified) {
		this.modified = modified;
	}

	public ModelUtilisateur(List<Utilisateur> utilisateurs) {
		liste = utilisateurs;
		
		
	}
	
	@Override
	public int getRowCount() {
		return liste == null ? 0 : liste.size();
	}

	@Override
	public int getColumnCount() {
		return 2;
	}

	@Override
	public String getColumnName(int column) {
		return colNames[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column !=0);
	}

	@Override
	public Object getValueAt(int row, int column) {
		Object value =null;
		Utilisateur m = liste.get(row);
		switch (column) {
		case 0 : 
			value = m.getUtilisateur();
			break;
		case 1 :
			value = m.getRoleNom();
			break;
		
		
		}
		
		
		return value;
		
		
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		// TODO Auto-generated method stub
		Utilisateur m = liste.get(row);
		modified.add(m);
		
		
        
         
       
	}

	@Override
	public Class getColumnClass(int column)
    {
        for (int row = 0; row < getRowCount(); row++)
        {
            Object o = getValueAt(row, column);

            if (o != null)
            {
                return o.getClass();
            }
        }

        return Object.class;
    
}
}

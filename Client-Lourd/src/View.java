
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.HeadlessException;
import java.awt.event.MouseListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.BevelBorder;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTable;
import java.awt.Color;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import Entities.Commercial;
import Entities.Laboratoire;
import Entities.Medecin;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.Component;
import java.awt.Event;

import javax.swing.JScrollPane;
import java.awt.SystemColor;
import javax.swing.border.MatteBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class View extends JFrame {

	private JPanel contentPane;
	public JTextField textField;
	private JTextField textField_1;
	private JButton btnNewButton;
	private JComboBox comboBox;
	private JTextField textField_2;
	private JTable table;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;
	private JButton btnNewButton_3;
	private JButton btnNewButton_4;
	private JTextField textField_3;
	private JButton btnNewButton_5;
	private JPanel panel_5;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JTextField textField_4;
	private JLabel lblNewLabel_4;
	private JTextField textField_5;
	private JLabel lblNewLabel_5;
	private JTextField textField_6;
	private JLabel lblNewLabel_6;
	private JTextField textField_7;
	private JLabel lblNewLabel_7;
	private JTextField textField_8;
	private JLabel lblNewLabel_8;
	private JTextField textField_9;
	private JButton btnNewButton_6;
	private JButton btnNewButton_7;
	private JButton btnNewButton_8;
	private JButton btnNewButton_9;
	private JButton btnNewButton_10;
	private JTextField textField_10;
	private JButton btnNewButton_11;
	private JTable table_1;
	private JPanel panel_6;
	private JPanel panel_7;
	private JPanel panel_8;
	private JScrollPane scrollPane_2;
	private JTable table_2;
	private JLabel lblNewLabel_9;
	private JLabel lblNewLabel_10;
	private JLabel lblNewLabel_11;
	private JLabel lblNewLabel_12;
	private JLabel lblNewLabel_13;
	private JLabel lblNewLabel_14;
	private JLabel lblNewLabel_15;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JButton btnNewButton_13;
	private JButton btnNewButton_14;
	private JButton btnNewButton_15;
	private JButton btnNewButton_16;
	private JTextField textField_17;
	private JButton btnNewButton_17;
	private JButton btnNewButton_12;
	private JPanel panel_9;
	private JPanel panel_10;
	private JScrollPane scrollPane_3;
	private JTable table_3;
	private JLabel lblNewLabel_16;
	private JComboBox comboBox_1;
	private JLabel lblNewLabel_17;
	private JTextField textField_16;
	private JLabel lblNewLabel_18;
	private JLabel lblNewLabel_19;
	private JTextField textField_18;
	private JButton btnNewButton_18;
	private JButton btnNewButton_19;
	private JButton btnNewButton_20;
	private JTextField textField_19;
	private JButton btnNewButton_21;
	private JButton btnNewButton_22;
	private JButton btnNewButton_23;
	private JScrollPane scrollPane_4;
	private JTextArea textArea;
	private JPanel panel_12;
	private JLabel lblNewLabel_20;
	private JPanel panel_13;
	private JTable table_4;
	private JPanel panel_14;
	private JPanel panel_15;
	private JPanel panel_16;
	private JPanel panel_17;
	private JPanel panel_18;
	private JScrollPane scrollPane_6;
	private JTable table_5;
	private JTabbedPane tabbedPane;
	private JPanel panel_19;
	private JLabel lblNewLabel_21;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View frame = new View();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public View() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 975, 649);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setForeground(Color.BLACK);
		tabbedPane.setBackground(Color.WHITE);
		tabbedPane.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.BLACK);
		panel.setFont(new Font("Tahoma", Font.BOLD, 16));
		tabbedPane.addTab("Laboratoires", null, panel, null);
		
		JPanel panel_4 = new JPanel();
		panel_4.setForeground(Color.WHITE);
		panel_4.setBackground(Color.BLACK);
		panel_4.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, null, null, null));
		
		JScrollPane scrollPane = new JScrollPane();
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		
		btnNewButton_5 = new JButton("Rechercher");
		btnNewButton_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_5.setBackground(Color.WHITE);
		btnNewButton_5.setFont(new Font("Tahoma", Font.BOLD, 14));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addComponent(panel_4, GroupLayout.PREFERRED_SIZE, 368, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_5)
							.addGap(133))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 586, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)))
					.addGap(10))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addComponent(panel_4, GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_5))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 429, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(113, Short.MAX_VALUE))
		);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
			}
		));
		table.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		table.setForeground(Color.BLACK);
		table.setSelectionBackground(Color.BLACK);
		table.setFont(new Font("Tahoma", Font.BOLD, 14));
		table.setBackground(Color.WHITE);
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("Laboratoires");
		lblNewLabel.setBackground(Color.LIGHT_GRAY);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblNewLabel_1 = new JLabel("Nom du Laboratoire");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lblNewLabel_1_1 = new JLabel("Adresse du Laboratoire");
		lblNewLabel_1_1.setForeground(Color.WHITE);
		lblNewLabel_1_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lblNewLabel_1_2 = new JLabel("Ville du Laboratoire");
		lblNewLabel_1_2.setForeground(Color.WHITE);
		lblNewLabel_1_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textField_1.setColumns(10);
		
		comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"", "Lyon", "Paris", "Marseille", "Grenoble", "Toulouse", "Bordeaux"}));
		
		JLabel lblNewLabel_1_3 = new JLabel("Identifiant du Laboratoire");
		lblNewLabel_1_3.setForeground(Color.WHITE);
		lblNewLabel_1_3.setFont(new Font("Tahoma", Font.BOLD, 10));
		
		textField_2 = new JTextField();
		textField_2.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textField_2.setColumns(10);
		
		btnNewButton_3 = new JButton("Vider");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_3.setFont(new Font("Tahoma", Font.BOLD, 10));
		btnNewButton_3.setVisible(false);
		
		btnNewButton_4 = new JButton("Afficher le laboratoire");
		btnNewButton_4.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton_4.setVisible(false);
		
		panel_14 = new JPanel();
		panel_14.setBackground(Color.BLACK);
		
		btnNewButton = new JButton("Ajouter");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(0, 0, 128));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		gl_panel_4.setHorizontalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_4.createSequentialGroup()
							.addGap(102)
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_3))
						.addGroup(gl_panel_4.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
							.addGap(33)
							.addComponent(textField, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(34, Short.MAX_VALUE))
				.addGroup(gl_panel_4.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_4.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel_4.createSequentialGroup()
							.addComponent(lblNewLabel_1_1, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_4.createSequentialGroup()
							.addGroup(gl_panel_4.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel_1_3, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
								.addComponent(lblNewLabel_1_2, GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
								.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE))))
					.addGap(32))
				.addGroup(gl_panel_4.createSequentialGroup()
					.addGap(76)
					.addComponent(btnNewButton_4, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(92, Short.MAX_VALUE))
				.addGroup(gl_panel_4.createSequentialGroup()
					.addContainerGap(17, Short.MAX_VALUE)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_14, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel_4.setVerticalGroup(
			gl_panel_4.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_4.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(btnNewButton_3))
					.addGap(31)
					.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(46)
					.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1_1, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(51)
					.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1_2, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(53)
					.addGroup(gl_panel_4.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1_3, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_4.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_4.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_14, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_4.createSequentialGroup()
							.addGap(33)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_4, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addGap(70))
		);
		
		btnNewButton_1 = new JButton("Modifier");
		btnNewButton_1.setBackground(Color.WHITE);
		btnNewButton_1.setVisible(false);
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		
		btnNewButton_2 = new JButton("Supprimer");
		btnNewButton_2.setForeground(Color.WHITE);
		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_2.setVisible(false);
		btnNewButton_2.setBackground(Color.RED);
		GroupLayout gl_panel_14 = new GroupLayout(panel_14);
		gl_panel_14.setHorizontalGroup(
			gl_panel_14.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_14.createSequentialGroup()
					.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_2)
					.addContainerGap(113, Short.MAX_VALUE))
		);
		gl_panel_14.setVerticalGroup(
			gl_panel_14.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_14.createSequentialGroup()
					.addGap(30)
					.addGroup(gl_panel_14.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(42, Short.MAX_VALUE))
		);
		panel_14.setLayout(gl_panel_14);
		panel_4.setLayout(gl_panel_4);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		tabbedPane.addTab("Medecins", null, panel_1, null);
		
		panel_5 = new JPanel();
		panel_5.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_5.setForeground(Color.WHITE);
		panel_5.setBackground(Color.BLACK);
		
		lblNewLabel_2 = new JLabel("M\u00E9decins");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		lblNewLabel_3 = new JLabel("Nom du m\u00E9d\u00E9cin");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		
		lblNewLabel_4 = new JLabel("Pr\u00E9nom du m\u00E9decin");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		
		lblNewLabel_5 = new JLabel("Mail du m\u00E9decin");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_5.setForeground(Color.WHITE);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		
		lblNewLabel_6 = new JLabel("T\u00E9l\u00E9phone du m\u00E9decin");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setBackground(new Color(240, 240, 240));
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		
		lblNewLabel_7 = new JLabel("Adresse du m\u00E9decin");
		lblNewLabel_7.setForeground(Color.WHITE);
		lblNewLabel_7.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		
		lblNewLabel_8 = new JLabel("Identifiant du m\u00E9decin");
		lblNewLabel_8.setForeground(Color.WHITE);
		lblNewLabel_8.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		
		btnNewButton_9 = new JButton("Afficher le m\u00E9decin");
		btnNewButton_9.setBackground(new Color(240, 240, 240));
		btnNewButton_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_9.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton_9.setVisible(false);
		
		btnNewButton_10 = new JButton("Vider");
		btnNewButton_10.setFont(new Font("Tahoma", Font.BOLD, 10));
		btnNewButton_10.setVisible(false);
		
		panel_15 = new JPanel();
		panel_15.setBackground(Color.BLACK);
		
		btnNewButton_6 = new JButton("Ajouter");
		btnNewButton_6.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_6.setForeground(Color.WHITE);
		btnNewButton_6.setBackground(new Color(0, 0, 128));
		GroupLayout gl_panel_5 = new GroupLayout(panel_5);
		gl_panel_5.setHorizontalGroup(
			gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
					.addContainerGap(108, Short.MAX_VALUE)
					.addComponent(lblNewLabel_2, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_10, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(85))
				.addGroup(gl_panel_5.createSequentialGroup()
					.addGap(22)
					.addComponent(lblNewLabel_5, GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_panel_5.createSequentialGroup()
					.addGap(22)
					.addGroup(gl_panel_5.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNewLabel_8, GroupLayout.DEFAULT_SIZE, 338, Short.MAX_VALUE)
						.addGroup(gl_panel_5.createSequentialGroup()
							.addComponent(lblNewLabel_6, GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(gl_panel_5.createSequentialGroup()
							.addGroup(gl_panel_5.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblNewLabel_7, GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)
								.addGroup(gl_panel_5.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(lblNewLabel_4, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(lblNewLabel_3, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE)))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(gl_panel_5.createParallelGroup(Alignment.LEADING)
								.addComponent(textField_8, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
								.addComponent(textField_7, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
								.addComponent(textField_9, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
								.addComponent(textField_6, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
								.addComponent(textField_4, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
								.addComponent(textField_5, GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE))
							.addGap(39))))
				.addGroup(gl_panel_5.createSequentialGroup()
					.addGap(87)
					.addComponent(btnNewButton_9, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(76, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel_5.createSequentialGroup()
					.addContainerGap(15, Short.MAX_VALUE)
					.addComponent(btnNewButton_6, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_15, GroupLayout.PREFERRED_SIZE, 232, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel_5.setVerticalGroup(
			gl_panel_5.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_5.createSequentialGroup()
					.addGap(12)
					.addGroup(gl_panel_5.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblNewLabel_2)
						.addComponent(btnNewButton_10))
					.addGap(41)
					.addGroup(gl_panel_5.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_3)
						.addComponent(textField_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(37)
					.addGroup(gl_panel_5.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_4)
						.addComponent(textField_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(36)
					.addGroup(gl_panel_5.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_5)
						.addComponent(textField_6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(34)
					.addGroup(gl_panel_5.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_6)
						.addComponent(textField_7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(35)
					.addGroup(gl_panel_5.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_7)
						.addComponent(textField_8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(38)
					.addGroup(gl_panel_5.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_8)
						.addComponent(textField_9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_5.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_5.createSequentialGroup()
							.addGap(10)
							.addComponent(panel_15, GroupLayout.DEFAULT_SIZE, 99, Short.MAX_VALUE))
						.addGroup(gl_panel_5.createSequentialGroup()
							.addGap(36)
							.addComponent(btnNewButton_6, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_9, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
					.addGap(59))
		);
		
		btnNewButton_7 = new JButton("Modifier");
		btnNewButton_7.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_7.setBackground(Color.WHITE);
		
		
		btnNewButton_7.setVisible(false);
		
		btnNewButton_8 = new JButton("Supprimer");
		btnNewButton_8.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnNewButton_8.setBackground(Color.RED);
		btnNewButton_8.setForeground(Color.WHITE);
		btnNewButton_8.setVisible(false);
		GroupLayout gl_panel_15 = new GroupLayout(panel_15);
		gl_panel_15.setHorizontalGroup(
			gl_panel_15.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_15.createSequentialGroup()
					.addComponent(btnNewButton_7, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_8, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(113, Short.MAX_VALUE))
		);
		gl_panel_15.setVerticalGroup(
			gl_panel_15.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_15.createSequentialGroup()
					.addGap(26)
					.addGroup(gl_panel_15.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_7, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_8, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(37, Short.MAX_VALUE))
		);
		panel_15.setLayout(gl_panel_15);
		panel_5.setLayout(gl_panel_5);
		
		textField_10 = new JTextField();
		textField_10.setColumns(10);
		
		btnNewButton_11 = new JButton("Rechercher");
		btnNewButton_11.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addComponent(panel_5, GroupLayout.PREFERRED_SIZE, 364, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(139)
							.addComponent(textField_10, GroupLayout.PREFERRED_SIZE, 231, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_11, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 582, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField_10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_11))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(88, Short.MAX_VALUE))
		);
		
		table_1 = new JTable();
		table_1.setFont(new Font("Tahoma", Font.BOLD, 14));
		scrollPane_1.setViewportView(table_1);
		panel_1.setLayout(gl_panel_1);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Commerciaux", null, panel_2, null);
		
		panel_6 = new JPanel();
		panel_6.setForeground(Color.WHITE);
		panel_6.setBackground(Color.BLACK);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addComponent(panel_6, GroupLayout.DEFAULT_SIZE, 946, Short.MAX_VALUE)
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addComponent(panel_6, GroupLayout.DEFAULT_SIZE, 584, Short.MAX_VALUE)
		);
		
		panel_7 = new JPanel();
		panel_7.setBackground(Color.BLACK);
		panel_7.setForeground(Color.WHITE);
		
		panel_8 = new JPanel();
		
		textField_17 = new JTextField();
		textField_17.setColumns(10);
		
		btnNewButton_17 = new JButton("Rechercher");
		GroupLayout gl_panel_6 = new GroupLayout(panel_6);
		gl_panel_6.setHorizontalGroup(
			gl_panel_6.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_6.createSequentialGroup()
					.addComponent(panel_7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_panel_6.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_6.createSequentialGroup()
							.addGap(142)
							.addComponent(textField_17, GroupLayout.PREFERRED_SIZE, 229, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_17, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_6.createSequentialGroup()
							.addGap(6)
							.addComponent(panel_8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel_6.setVerticalGroup(
			gl_panel_6.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_6.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_6.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_17))
					.addPreferredGap(ComponentPlacement.RELATED, 100, Short.MAX_VALUE)
					.addComponent(panel_8, GroupLayout.PREFERRED_SIZE, 429, GroupLayout.PREFERRED_SIZE)
					.addGap(116))
				.addComponent(panel_7, GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
		);
		
		lblNewLabel_9 = new JLabel("Commerciaux");
		lblNewLabel_9.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_9.setForeground(Color.WHITE);
		lblNewLabel_9.setBackground(Color.BLACK);
		
		lblNewLabel_10 = new JLabel("Nom de l'employ\u00E9");
		lblNewLabel_10.setBackground(Color.BLACK);
		lblNewLabel_10.setForeground(Color.WHITE);
		lblNewLabel_10.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		lblNewLabel_11 = new JLabel("Pr\u00E9nom de l'employ\u00E9");
		lblNewLabel_11.setForeground(Color.WHITE);
		lblNewLabel_11.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_11.setBackground(Color.BLACK);
		
		lblNewLabel_12 = new JLabel("Mail de l'employ\u00E9");
		lblNewLabel_12.setForeground(Color.WHITE);
		lblNewLabel_12.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_12.setBackground(Color.BLACK);
		
		lblNewLabel_13 = new JLabel("T\u00E9l\u00E9phonede l'employ\u00E9");
		lblNewLabel_13.setForeground(Color.WHITE);
		lblNewLabel_13.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_13.setBackground(Color.BLACK);
		
		lblNewLabel_14 = new JLabel("Identifiant de l'employ\u00E9");
		lblNewLabel_14.setForeground(Color.WHITE);
		lblNewLabel_14.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_14.setBackground(Color.BLACK);
		
		lblNewLabel_15 = new JLabel("Laboratoire de l'employ\u00E9");
		lblNewLabel_15.setForeground(Color.WHITE);
		lblNewLabel_15.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblNewLabel_15.setBackground(Color.BLACK);
		
		textField_11 = new JTextField();
		textField_11.setColumns(10);
		
		textField_12 = new JTextField();
		textField_12.setColumns(10);
		
		textField_13 = new JTextField();
		textField_13.setColumns(10);
		
		textField_14 = new JTextField();
		textField_14.setColumns(10);
		
		textField_15 = new JTextField();
		textField_15.setColumns(10);
		
		btnNewButton_15 = new JButton("Afficher le commercial");
		btnNewButton_15.setForeground(Color.BLACK);
		btnNewButton_15.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton_15.setBackground(Color.LIGHT_GRAY);
		btnNewButton_15.setVisible(false);
		
		btnNewButton_16 = new JButton("Vider");
		btnNewButton_16.setVisible(false);
		
		comboBox_1 = new JComboBox();
		
		panel_16 = new JPanel();
		panel_16.setBackground(Color.BLACK);
		
		btnNewButton_12 = new JButton("Ajouter");
		btnNewButton_12.setBackground(new Color(0, 0, 128));
		btnNewButton_12.setForeground(Color.WHITE);
		btnNewButton_12.setFont(new Font("Tahoma", Font.BOLD, 15));
		GroupLayout gl_panel_7 = new GroupLayout(panel_7);
		gl_panel_7.setHorizontalGroup(
			gl_panel_7.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_7.createSequentialGroup()
					.addGroup(gl_panel_7.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_7.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel_15, GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_7.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel_10, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
							.addComponent(textField_11, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_7.createSequentialGroup()
							.addGap(95)
							.addComponent(lblNewLabel_9, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNewButton_16)
							.addPreferredGap(ComponentPlacement.RELATED, 38, Short.MAX_VALUE))
						.addGroup(gl_panel_7.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel_7.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_panel_7.createSequentialGroup()
									.addComponent(lblNewLabel_11, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
									.addComponent(textField_12, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_7.createSequentialGroup()
									.addComponent(lblNewLabel_12, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
									.addComponent(textField_13, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_7.createSequentialGroup()
									.addComponent(lblNewLabel_13, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
									.addComponent(textField_14, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel_7.createSequentialGroup()
									.addComponent(lblNewLabel_14, GroupLayout.DEFAULT_SIZE, 145, Short.MAX_VALUE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(textField_15, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE)))))
					.addGap(29))
				.addGroup(gl_panel_7.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnNewButton_12, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_16, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(15, Short.MAX_VALUE))
				.addGroup(gl_panel_7.createSequentialGroup()
					.addGap(69)
					.addComponent(btnNewButton_15, GroupLayout.PREFERRED_SIZE, 213, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(84, Short.MAX_VALUE))
		);
		gl_panel_7.setVerticalGroup(
			gl_panel_7.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_7.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_7.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_9)
						.addComponent(btnNewButton_16))
					.addGap(38)
					.addGroup(gl_panel_7.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_10)
						.addComponent(textField_11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(34)
					.addGroup(gl_panel_7.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_11, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(41)
					.addGroup(gl_panel_7.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_12, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(39)
					.addGroup(gl_panel_7.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_13, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(39)
					.addGroup(gl_panel_7.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_14, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(textField_15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(36)
					.addGroup(gl_panel_7.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_15, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGroup(gl_panel_7.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_7.createSequentialGroup()
							.addGap(28)
							.addComponent(btnNewButton_12, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_7.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_16, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnNewButton_15, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(77, Short.MAX_VALUE))
		);
		
		btnNewButton_13 = new JButton("Modifier");
		btnNewButton_13.setForeground(Color.BLACK);
		btnNewButton_13.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnNewButton_13.setBackground(Color.WHITE);
		btnNewButton_13.setVisible(false);
		
		btnNewButton_14 = new JButton("Supprimer");
		btnNewButton_14.setForeground(Color.WHITE);
		btnNewButton_14.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_14.setBackground(Color.RED);
		btnNewButton_14.setVisible(false);
		GroupLayout gl_panel_16 = new GroupLayout(panel_16);
		gl_panel_16.setHorizontalGroup(
			gl_panel_16.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_16.createSequentialGroup()
					.addComponent(btnNewButton_13, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_14, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		gl_panel_16.setVerticalGroup(
			gl_panel_16.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_16.createSequentialGroup()
					.addGap(21)
					.addGroup(gl_panel_16.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_13, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_14, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(29, Short.MAX_VALUE))
		);
		panel_16.setLayout(gl_panel_16);
		panel_7.setLayout(gl_panel_7);
		
		scrollPane_2 = new JScrollPane();
		GroupLayout gl_panel_8 = new GroupLayout(panel_8);
		gl_panel_8.setHorizontalGroup(
			gl_panel_8.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane_2, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 583, Short.MAX_VALUE)
		);
		gl_panel_8.setVerticalGroup(
			gl_panel_8.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane_2, GroupLayout.DEFAULT_SIZE, 429, Short.MAX_VALUE)
		);
		
		table_2 = new JTable();
		table_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		scrollPane_2.setViewportView(table_2);
		panel_8.setLayout(gl_panel_8);
		panel_6.setLayout(gl_panel_6);
		panel_2.setLayout(gl_panel_2);
		
		JPanel panel_3 = new JPanel();
		panel_3.setForeground(Color.WHITE);
		panel_3.setBackground(Color.BLACK);
		tabbedPane.addTab("M\u00E9dicaments", null, panel_3, null);
		
		panel_9 = new JPanel();
		panel_9.setBackground(Color.BLACK);
		panel_9.setForeground(Color.WHITE);
		
		panel_10 = new JPanel();
		panel_10.setForeground(Color.WHITE);
		panel_10.setBackground(Color.BLACK);
		
		textField_19 = new JTextField();
		textField_19.setColumns(10);
		
		btnNewButton_23 = new JButton("Rechercher");
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3.setHorizontalGroup(
			gl_panel_3.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addComponent(panel_9, GroupLayout.PREFERRED_SIZE, 359, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_3.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel_10, GroupLayout.DEFAULT_SIZE, 581, Short.MAX_VALUE))
						.addGroup(gl_panel_3.createSequentialGroup()
							.addGap(121)
							.addComponent(textField_19, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnNewButton_23, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())))
		);
		gl_panel_3.setVerticalGroup(
			gl_panel_3.createParallelGroup(Alignment.TRAILING)
				.addComponent(panel_9, GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE)
				.addGroup(gl_panel_3.createSequentialGroup()
					.addContainerGap(224, Short.MAX_VALUE)
					.addGroup(gl_panel_3.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_23))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_10, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE))
		);
		
		lblNewLabel_16 = new JLabel("M\u00E9dicaments");
		lblNewLabel_16.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_16.setForeground(Color.WHITE);
		lblNewLabel_16.setBackground(Color.BLACK);
		
		lblNewLabel_17 = new JLabel("Nom du m\u00E9dicament");
		lblNewLabel_17.setForeground(Color.WHITE);
		lblNewLabel_17.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		textField_16 = new JTextField();
		textField_16.setColumns(10);
		
		lblNewLabel_18 = new JLabel("Description du m\u00E9dicament :");
		lblNewLabel_18.setForeground(Color.WHITE);
		lblNewLabel_18.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		lblNewLabel_19 = new JLabel("Identifiant du m\u00E9dicament");
		lblNewLabel_19.setForeground(Color.WHITE);
		lblNewLabel_19.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		textField_18 = new JTextField();
		textField_18.setColumns(10);
		
		btnNewButton_21 = new JButton("Afficher le m\u00E9dicament");
		btnNewButton_21.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		btnNewButton_22 = new JButton("Vider");
		btnNewButton_21.setVisible(false);
		btnNewButton_22.setVisible(false);
		
		scrollPane_4 = new JScrollPane();
		
		panel_17 = new JPanel();
		panel_17.setBackground(Color.BLACK);
		
		btnNewButton_18 = new JButton("Ajouter");
		btnNewButton_18.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_18.setBackground(new Color(0, 0, 128));
		btnNewButton_18.setForeground(Color.WHITE);
		GroupLayout gl_panel_9 = new GroupLayout(panel_9);
		gl_panel_9.setHorizontalGroup(
			gl_panel_9.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_9.createSequentialGroup()
					.addGroup(gl_panel_9.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_9.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel_16)
							.addGap(18)
							.addComponent(btnNewButton_22))
						.addGroup(gl_panel_9.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel_17, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(textField_16, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
				.addGroup(gl_panel_9.createSequentialGroup()
					.addGap(27)
					.addComponent(scrollPane_4, GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(38, Short.MAX_VALUE))
				.addGroup(gl_panel_9.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_18, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(173, Short.MAX_VALUE))
				.addGroup(gl_panel_9.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnNewButton_18, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_17, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(gl_panel_9.createSequentialGroup()
					.addGap(74)
					.addComponent(btnNewButton_21, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(84, Short.MAX_VALUE))
				.addGroup(gl_panel_9.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_19, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(185, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel_9.createSequentialGroup()
					.addContainerGap(135, Short.MAX_VALUE)
					.addComponent(textField_18, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
					.addGap(72))
		);
		gl_panel_9.setVerticalGroup(
			gl_panel_9.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_9.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_9.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_16)
						.addComponent(btnNewButton_22))
					.addGap(27)
					.addGroup(gl_panel_9.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_17)
						.addComponent(textField_16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(51)
					.addComponent(lblNewLabel_18)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane_4, GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
					.addGap(14)
					.addComponent(lblNewLabel_19)
					.addGroup(gl_panel_9.createParallelGroup(Alignment.LEADING, false)
						.addGroup(gl_panel_9.createSequentialGroup()
							.addGap(2)
							.addComponent(textField_18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(panel_17, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
							.addGap(10))
						.addGroup(Alignment.TRAILING, gl_panel_9.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(btnNewButton_18, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
							.addGap(46)))
					.addComponent(btnNewButton_21, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addGap(82))
		);
		
		btnNewButton_19 = new JButton("Modifier");
		btnNewButton_19.setForeground(new Color(0, 0, 0));
		btnNewButton_19.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_19.setBackground(new Color(255, 255, 255));
		
		btnNewButton_19.setVisible(false);
		
		btnNewButton_20 = new JButton("Supprimer");
		btnNewButton_20.setForeground(new Color(255, 255, 255));
		btnNewButton_20.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton_20.setBackground(new Color(255, 0, 0));
		btnNewButton_20.setVisible(false);
		GroupLayout gl_panel_17 = new GroupLayout(panel_17);
		gl_panel_17.setHorizontalGroup(
			gl_panel_17.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panel_17.createSequentialGroup()
					.addComponent(btnNewButton_19, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
					.addComponent(btnNewButton_20, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_panel_17.setVerticalGroup(
			gl_panel_17.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_17.createSequentialGroup()
					.addGap(28)
					.addGroup(gl_panel_17.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_20, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_19, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(36, Short.MAX_VALUE))
		);
		panel_17.setLayout(gl_panel_17);
		
		textArea = new JTextArea();
		scrollPane_4.setViewportView(textArea);
		panel_9.setLayout(gl_panel_9);
		
		scrollPane_3 = new JScrollPane();
		GroupLayout gl_panel_10 = new GroupLayout(panel_10);
		gl_panel_10.setHorizontalGroup(
			gl_panel_10.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane_3, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 581, Short.MAX_VALUE)
		);
		gl_panel_10.setVerticalGroup(
			gl_panel_10.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_10.createSequentialGroup()
					.addComponent(scrollPane_3, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(151, Short.MAX_VALUE))
		);
		
		
		this.table_3 = new JTable(new DefaultTableModel()) {
			 @Override
			 public TableCellRenderer getCellRenderer(int row, int column)    {
			   TableCellRenderer renderer = getDefaultRenderer(dataModel.getValueAt(convertRowIndexToModel(row), convertColumnIndexToModel(column)).getClass());
				if(renderer == null)	{
					renderer = getDefaultRenderer(Object.class);
					if(renderer == null)	{
						renderer = new DefaultTableCellRenderer();
						setDefaultRenderer(Object.class, renderer);				
					}
				}
				return renderer;
		    }

			 @Override
			 public TableCellEditor getCellEditor(int row, int column)    {
				TableCellEditor editor = getDefaultEditor(dataModel.getValueAt(convertRowIndexToModel(row), convertColumnIndexToModel(column)).getClass());
				if(editor == null)	{
					editor = getDefaultEditor(Object.class);
					if(editor == null)	{
						editor = new DefaultCellEditor(new JTextField());
						setDefaultEditor(Object.class, editor);						
					}
				}
				return editor;
			 }

		};
		table_3.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
				}
			));
			table_3.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
			table_3.setForeground(Color.BLACK);
			table_3.setSelectionBackground(Color.BLACK);
			table_3.setFont(new Font("Tahoma", Font.BOLD, 14));
			table_3.setBackground(Color.WHITE);
			
		scrollPane_3.setViewportView(table_3);
		panel_10.setLayout(gl_panel_10);
		panel_3.setLayout(gl_panel_3);
		
		JPanel panel_11 = new JPanel();
		panel_11.setToolTipText("");
		tabbedPane.addTab("Rendez-Vous", null, panel_11, null);
		
		panel_12 = new JPanel();
		panel_12.setForeground(Color.WHITE);
		panel_12.setBackground(Color.BLACK);
		
		panel_13 = new JPanel();
		panel_13.setBackground(Color.BLACK);
		GroupLayout gl_panel_11 = new GroupLayout(panel_11);
		gl_panel_11.setHorizontalGroup(
			gl_panel_11.createParallelGroup(Alignment.LEADING)
				.addComponent(panel_12, GroupLayout.DEFAULT_SIZE, 946, Short.MAX_VALUE)
				.addComponent(panel_13, GroupLayout.DEFAULT_SIZE, 946, Short.MAX_VALUE)
		);
		gl_panel_11.setVerticalGroup(
			gl_panel_11.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_11.createSequentialGroup()
					.addComponent(panel_12, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_13, GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE))
		);
		
		JScrollPane scrollPane_5 = new JScrollPane();
		GroupLayout gl_panel_13 = new GroupLayout(panel_13);
		gl_panel_13.setHorizontalGroup(
			gl_panel_13.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_13.createSequentialGroup()
					.addGap(120)
					.addComponent(scrollPane_5, GroupLayout.PREFERRED_SIZE, 583, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(243, Short.MAX_VALUE))
		);
		gl_panel_13.setVerticalGroup(
			gl_panel_13.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_13.createSequentialGroup()
					.addGap(33)
					.addComponent(scrollPane_5, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(185, Short.MAX_VALUE))
		);
		
		table_4 = new JTable();
		scrollPane_5.setViewportView(table_4);
		panel_13.setLayout(gl_panel_13);
		
		lblNewLabel_20 = new JLabel("Liste des Rendez-Vous entre m\u00E9decins et commerciaux");
		lblNewLabel_20.setForeground(Color.WHITE);
		lblNewLabel_20.setFont(new Font("Tahoma", Font.BOLD, 15));
		GroupLayout gl_panel_12 = new GroupLayout(panel_12);
		gl_panel_12.setHorizontalGroup(
			gl_panel_12.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_12.createSequentialGroup()
					.addGap(239)
					.addComponent(lblNewLabel_20)
					.addContainerGap(286, Short.MAX_VALUE))
		);
		gl_panel_12.setVerticalGroup(
			gl_panel_12.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_12.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel_20)
					.addContainerGap(11, Short.MAX_VALUE))
		);
		panel_12.setLayout(gl_panel_12);
		panel_11.setLayout(gl_panel_11);
		
		panel_18 = new JPanel();
		panel_18.setBackground(Color.BLACK);
		tabbedPane.addTab("Gestion des Roles", null, panel_18, null);
		
		
		scrollPane_6 = new JScrollPane();
		
		panel_19 = new JPanel();
		panel_19.setBackground(Color.BLACK);
		GroupLayout gl_panel_18 = new GroupLayout(panel_18);
		gl_panel_18.setHorizontalGroup(
			gl_panel_18.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_18.createSequentialGroup()
					.addGap(101)
					.addComponent(scrollPane_6, GroupLayout.PREFERRED_SIZE, 503, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(342, Short.MAX_VALUE))
				.addComponent(panel_19, GroupLayout.DEFAULT_SIZE, 946, Short.MAX_VALUE)
		);
		gl_panel_18.setVerticalGroup(
			gl_panel_18.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_18.createSequentialGroup()
					.addComponent(panel_19, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
					.addGap(33)
					.addComponent(scrollPane_6, GroupLayout.PREFERRED_SIZE, 292, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(199, Short.MAX_VALUE))
		);
		
		lblNewLabel_21 = new JLabel("Gestion des r\u00F4les des utilisateurs");
		lblNewLabel_21.setForeground(Color.WHITE);
		lblNewLabel_21.setFont(new Font("Tahoma", Font.BOLD, 19));
		panel_19.add(lblNewLabel_21);
		
		table_5 = new JTable();
		scrollPane_6.setViewportView(table_5);
		panel_18.setLayout(gl_panel_18);
	}

	

	public void addMouseListener(MouseListener l) {
		table.addMouseListener(l);
	}

	

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public JTextField getTextField_1() {
		return textField_1;
	}

	public void setTextField_2(JTextField textField_2) {
		this.textField_2 = textField_2;
	}
	
	

	public void setTextField_1(JTextField textField_1) {
		this.textField_1 = textField_1;
	}

	
	public JButton getBtnNewButton() {
		return btnNewButton;
	}
	public JComboBox getComboBox() {
		return comboBox;
	}
	public JTextField getTextField_2() {
		return textField_2;
	}
	public JButton getBtnNewButton_1() {
		return btnNewButton_1;
	}
	public JButton getBtnNewButton_2() {
		return btnNewButton_2;
	}
	
	public void clicker() throws  SQLException {
		
		List<Laboratoire> liste;
		int i = table.getSelectedRow();
		DefaultTableModel model =  (DefaultTableModel) table.getModel();
		
		textField_2.setText(model.getValueAt(i,0).toString());
		
		textField_2.setEditable(false);
		
		textField.setText(model.getValueAt(i,1).toString());
		
		textField.setEditable(false);
		
		comboBox.setSelectedItem(model.getValueAt(i,3).toString());
		
		comboBox.setEnabled(false);
		
		textField_1.setText(model.getValueAt(i,2).toString());
		
		textField_1.setEditable(false);
		
		
		
		
	}
	
	
	
	public JButton getBtnNewButton_3() {
		return btnNewButton_3;
	}
	public JButton getBtnNewButton_4() {
		return btnNewButton_4;
	}
	public JButton getBtnNewButton_5() {
		return btnNewButton_5;
	}
	public JTextField getTextField_3() {
		return textField_3;
	}
	public JButton getBtnNewButton_6() {
		return btnNewButton_6;
	}
	public JButton getBtnNewButton_7() {
		return btnNewButton_7;
	}
	public JButton getBtnNewButton_8() {
		return btnNewButton_8;
	}
	public JButton getBtnNewButton_10() {
		return btnNewButton_10;
	}
	public JButton getBtnNewButton_9() {
		return btnNewButton_9;
	}
	public JButton getBtnNewButton_11() {
		return btnNewButton_11;
	}
	public JTextField getTextField_4() {
		return textField_4;
	}
	public JTextField getTextField_5() {
		return textField_5;
	}
	public JLabel getLblNewLabel_5() {
		return lblNewLabel_5;
	}
	public JLabel getLblNewLabel_6() {
		return lblNewLabel_6;
	}
	public JTextField getTextField_8() {
		return textField_8;
	}
	public JLabel getLblNewLabel_8() {
		return lblNewLabel_8;
	}
	public JTextField getTextField_10() {
		return textField_10;
	}
	
	public JTextField getTextField_9() {
		return textField_9;
	}
	
	public JTable getTable_1() {
		return table_1;
	}
	
	public void clicker1() throws  SQLException {
			
			List<Medecin> liste;
			int i = table_1.getSelectedRow();
			DefaultTableModel model =  (DefaultTableModel) table_1.getModel();
			
			textField_9.setText(model.getValueAt(i,0).toString());
			
			textField_9.setEditable(false);
			
			textField_4.setText(model.getValueAt(i,1).toString());
			
			textField_4.setEditable(false);
			
			textField_5.setText(model.getValueAt(i,2).toString());
			
			textField_5.setEditable(false);
			
			textField_6.setText(model.getValueAt(i,3).toString());
			
			textField_6.setEditable(false);
			
			textField_7.setText(model.getValueAt(i,4).toString());
			
			textField_7.setEditable(false);
			
			textField_8.setText(model.getValueAt(i,5).toString());
			
			textField_8.setEditable(false);
			
			
			
			
		}
	
	public JTextField getTextField_6() {
		return textField_6;
	}
	public View(GraphicsConfiguration gc) {
		super(gc);
		// TODO Auto-generated constructor stub
	}

	public View(String title, GraphicsConfiguration gc) {
		super(title, gc);
		// TODO Auto-generated constructor stub
	}

	public View(String title) throws HeadlessException {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public JTextField getTextField_7() {
		return textField_7;
	}
	public JTextField getTextField_11() {
		return textField_11;
	}
	public JTextField getTextField_12() {
		return textField_12;
	}
	public JTextField getTextField_13() {
		return textField_13;
	}
	public JTextField getTextField_14() {
		return textField_14;
	}
	public JTextField getTextField_15() {
		return textField_15;
	}
	
	public JButton getBtnNewButton_12() {
		return btnNewButton_12;
	}
	public JButton getBtnNewButton_13() {
		return btnNewButton_13;
	}
	public JButton getBtnNewButton_16() {
		return btnNewButton_16;
	}
	public JButton getBtnNewButton_14() {
		return btnNewButton_14;
	}
	public JButton getBtnNewButton_15() {
		return btnNewButton_15;
	}
	public JTextField getTextField_17() {
		return textField_17;
	}
	public JButton getBtnNewButton_17() {
		return btnNewButton_17;
	}
	public JTable getTable_2() {
		return table_2;
	}
	
	public void clicker2() throws  SQLException {
		
		List<Commercial> liste;
		int i = table_2.getSelectedRow();
		DefaultTableModel model =  (DefaultTableModel) table_2.getModel();
		
		textField_15.setText(model.getValueAt(i,0).toString());
		
		textField_15.setEditable(false);
		
		textField_11.setText(model.getValueAt(i,1).toString());
		
		textField_11.setEditable(false);
		
		textField_12.setText(model.getValueAt(i,2).toString());
		
		textField_12.setEditable(false);
		
		textField_13.setText(model.getValueAt(i,3).toString());
		
		textField_13.setEditable(false);
		
		textField_14.setText(model.getValueAt(i,4).toString());
		
		textField_14.setEditable(false);
		
		comboBox_1.setSelectedItem(model.getValueAt(i,5).toString());
		
		comboBox_1.setEnabled(false);
		
		
		
		
	}
	public JComboBox getComboBox_1() {
		return comboBox_1;
	}
	public JTextField getTextField_16() {
		return textField_16;
	}
	public JTextArea getTextArea() {
		return textArea;
	}
	public JTextField getTextField_18() {
		return textField_18;
	}
	public JButton getBtnNewButton_18() {
		return btnNewButton_18;
	}
	public JButton getBtnNewButton_19() {
		return btnNewButton_19;
	}
	public JButton getBtnNewButton_20() {
		return btnNewButton_20;
	}
	public JButton getBtnNewButton_21() {
		return btnNewButton_21;
	}
	public JButton getBtnNewButton_22() {
		return btnNewButton_22;
	}
	public JTextField getTextField_19() {
		return textField_19;
	}
	public JButton getBtnNewButton_23() {
		return btnNewButton_23;
	}
	public JTable getTable_3() {
		return table_3;
	}
	
public void clicker3() throws  SQLException {
		
		
		int i = table_3.getSelectedRow();
		DefaultTableModel model =  (DefaultTableModel) table_3.getModel();
		
		textField_18.setText(model.getValueAt(i,0).toString());
		
		textField_18.setEditable(false);
		
		textField_16.setText(model.getValueAt(i,1).toString());
		
		textField_16.setEditable(false);
		
		textArea.setText(model.getValueAt(i,2).toString());
		
		textArea.setEditable(false);
		
		
		
		
		
		
	}
	public JTable getTable_4() {
		return table_4;
	}
	public JPanel getPanel_14() {
		return panel_14;
	}
	public JPanel getPanel_15() {
		return panel_15;
	}
	public JPanel getPanel_16() {
		return panel_16;
	}
	public JPanel getPanel_17() {
		return panel_17;
	}
	public JTable getTable_5() {
		return table_5;
	}
	public JPanel getPanel_18() {
		return panel_18;
	}
	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}
	
}

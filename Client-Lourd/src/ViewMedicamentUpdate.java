
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Window.Type;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpringLayout;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class ViewMedicamentUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JLabel lblNewLabel_1;
	private JScrollPane scrollPane;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewLaboratoireUpdate frame = new ViewLaboratoireUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewMedicamentUpdate() {
		setBounds(100, 100, 482, 619);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.BLACK);
		
		JLabel lblNewLabel = new JLabel("Modifier les informations du m\u00E9dicament");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setForeground(Color.WHITE);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JLabel lblNewLabel_2 = new JLabel("Identifiant du m\u00E9dicament");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_3 = new JLabel("Nom du m\u00E9dicament ");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_4 = new JLabel("Description du m\u00E9dicament :");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		textField = new JTextField();
		textField.setBackground(Color.WHITE);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		btnNewButton = new JButton("Modifier");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		btnNewButton_1 = new JButton("Annuler");
		btnNewButton_1.setBackground(Color.RED);
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		SpringLayout springLayout = new SpringLayout();
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_3, 14, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_3, -23, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.NORTH, textField_1, 0, SpringLayout.NORTH, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.WEST, textField_1, 0, SpringLayout.WEST, btnNewButton);
		springLayout.putConstraint(SpringLayout.EAST, textField_1, -31, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_3, -388, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_4, 52, SpringLayout.SOUTH, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_4, 0, SpringLayout.WEST, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 317, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 210, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, -6, SpringLayout.WEST, btnNewButton_1);
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_1, 0, SpringLayout.NORTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, 0, SpringLayout.SOUTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton_1, 418, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 421, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -59, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, textField, 38, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, textField, 210, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField, 427, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 41, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_2, 19, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_2, 192, SpringLayout.WEST, panel_1);
		panel_1.setLayout(springLayout);
		panel_1.add(btnNewButton);
		panel_1.add(btnNewButton_1);
		panel_1.add(lblNewLabel_4);
		panel_1.add(lblNewLabel_3);
		panel_1.add(lblNewLabel_2);
		panel_1.add(textField_1);
		panel_1.add(textField);
		
		scrollPane = new JScrollPane();
		springLayout.putConstraint(SpringLayout.NORTH, scrollPane, 27, SpringLayout.SOUTH, lblNewLabel_4);
		springLayout.putConstraint(SpringLayout.WEST, scrollPane, 80, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, scrollPane, 141, SpringLayout.SOUTH, lblNewLabel_4);
		springLayout.putConstraint(SpringLayout.EAST, scrollPane, 339, SpringLayout.WEST, panel_1);
		panel_1.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		contentPane.setLayout(gl_contentPane);
	}
	public JTextField getTextField() {
		return textField;
	}
	public JTextField getTextField_1() {
		return textField_1;
	}
	
	
	public JButton getBtnNewButton() {
		return btnNewButton;
	}
	public JButton getBtnNewButton_1() {
		return btnNewButton_1;
	}
	
	public JLabel getLblNewLabel_1() {
		return lblNewLabel_1;
	}
	
public void Afficher(View v) throws  SQLException {
		
		
		
		lblNewLabel_1.setText(v.getTextField_18().getText().toString());
		
		textField.setText(v.getTextField_18().getText().toString());
		
		textField_1.setText(v.getTextField_16().getText().toString());
		
		textArea.setText(v.getTextArea().getText().toString());
		
		
		
		
		
	}
	
	public JTextArea getTextArea() {
		return textArea;
	}
}

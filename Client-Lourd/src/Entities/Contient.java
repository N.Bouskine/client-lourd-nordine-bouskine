package Entities;

public class Contient {

	private String LaboId;
	private String MedicamentId;
	private String Quantite;
	
	public String getLaboId() {
		return LaboId;
	}
	public Contient setLaboId(String laboId) {
		LaboId = laboId;
		return this;
	}
	
	public String getMedicamentId() {
		return MedicamentId;
	}
	public Contient setMedicamentId(String medicamentId) {
		MedicamentId = medicamentId;
		return this;
	}
	
	public String getQuantite() {
		return Quantite;
	}
	public Contient setQuantite(String quantite) {
		Quantite = quantite;
		return this;
	}
}

package Entities;

public class Utilisateur {
	
	private String Utilisateur;
	private String RoleNom;
	
	public String getUtilisateur() {
		return Utilisateur;
	}
	public Utilisateur setUtilisateur(String utilisateur) {
		Utilisateur = utilisateur;
		return this;
	}
	public String getRoleNom() {
		return RoleNom;
	}
	public Utilisateur setRoleNom(String roleNom) {
		RoleNom = roleNom;
		return this;
	}
	
	
}

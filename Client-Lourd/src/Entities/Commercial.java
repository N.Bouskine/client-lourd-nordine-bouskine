package Entities;

public class Commercial {
	
	private String EmployeId;
	private String EmployeNom;
	private String EmployePrenom;
	private String EmployeMail;
	private String EmployeNumTel;
	private String LaboId;
	
	public String getEmployeId() {
		return EmployeId;
	}
	public Commercial setEmployeId(String employeId) {
		EmployeId = employeId;
		return this;
	}
	public String getEmployeNom() {
		return EmployeNom;
	}
	public Commercial setEmployeNom(String employeNom) {
		EmployeNom = employeNom;
		return this;
	}
	public String getEmployePrenom() {
		return EmployePrenom;
	}
	public Commercial setEmployePrenom(String employePrenom) {
		EmployePrenom = employePrenom;
		return this;
	}
	public String getEmployeMail() {
		return EmployeMail;
	}
	public Commercial setEmployeMail(String employeMail) {
		EmployeMail = employeMail;
		return this;
	}
	public String getEmployeNumTel() {
		return EmployeNumTel;
	}
	public Commercial setEmployeNumTel(String employeNumTel) {
		EmployeNumTel = employeNumTel;
		return this;
	}
	public String getLaboId() {
		return LaboId;
	}
	public Commercial setLaboId(String laboId) {
		LaboId = laboId;
		return this;
	}
	
	
	
	
	
	

}

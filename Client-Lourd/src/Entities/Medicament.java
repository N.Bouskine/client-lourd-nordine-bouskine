package Entities;

public class Medicament {
	
	
	private String MedicamentId;
	private String MedicamentNom;
	private String MedicamentDescription;
	
	
	public String getMedicamentId() {
		return MedicamentId;
	}
	public Medicament setMedicamentId(String medicamentId) {
		MedicamentId = medicamentId;
		return this;
	}
	public String getMedicamentNom() {
		return MedicamentNom;
	}
	public Medicament setMedicamentNom(String medicamentNom) {
		MedicamentNom = medicamentNom;
		return this;
	}
	public String getMedicamentDescription() {
		return MedicamentDescription;
	}
	public Medicament setMedicamentDescription(String medicamentDescription) {
		MedicamentDescription = medicamentDescription;
		return this;
	}
	
	
	
	
	

}

package Entities;

public class Role {
	
	private String roleId;
	private String roleNom;
	
	public String getRoleId() {
		return roleId;
	}
	public Role setRoleId(String roleId) {
		this.roleId = roleId;
		return this;
	}
	public String getRoleNom() {
		return roleNom;
	}
	public Role setRoleNom(String roleNom) {
		this.roleNom = roleNom;
		return this;
	}
	
	

}

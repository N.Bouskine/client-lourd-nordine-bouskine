package Entities;

public class Authentification {

	private String Utilisateur;
	private String Password;
	private String RoleId;
	
	public String getUtilisateur() {
		return Utilisateur;
	}
	public Authentification setUtilisateur(String utilisateur) {
		Utilisateur = utilisateur;
		return this;
	}
	public String getPassword() {
		return Password;
	}
	public Authentification setPassword(String password) {
		Password = password;
		return this;
	}
	public String getRoleId() {
		return RoleId;
	}
	public Authentification setRoleId(String roleId) {
		RoleId = roleId;
		return this;
	}
	
	
}

package Entities;

public class TableauRencontre {

	
	private String DateRencontre;
	private String HeureRencontre;
	
	
	
	public String getDateRencontre() {
		return DateRencontre;
	}
	public TableauRencontre setDateRencontre(String dateRencontre) {
		DateRencontre = dateRencontre;
		return this;
	}
	public String getHeureRencontre() {
		return HeureRencontre;
	}
	public TableauRencontre setHeureRencontre(String heureRencontre) {
		HeureRencontre = heureRencontre;
		return this;
	}

	
		
		private String MedecinNom;
		
		
		
		public String getMedecinNom() {
			return MedecinNom;
		}
		public TableauRencontre setMedecinNom(String medecinNom) {
			MedecinNom = medecinNom;
			return this;
		}
		
		
		
		private String EmployeNom;
		
		
		
		public String getEmployeNom() {
			return EmployeNom;
		}
		public TableauRencontre setEmployeNom(String employeNom) {
			EmployeNom = employeNom;
			return this;
		}
		
		

	

}

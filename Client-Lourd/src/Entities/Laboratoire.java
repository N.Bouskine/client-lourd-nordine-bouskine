package Entities;

public class Laboratoire {
	
	private String LaboCode;
	private String LaboNom;
	private String LaboAdresse;
	private String LaboVille;
	
	public String getLaboCode() {
		return LaboCode;
	}
	public Laboratoire setLaboCode(String laboCode) {
		LaboCode = laboCode;
		return this;
	}
	public String getLaboNom() {
		return LaboNom;
	}
	public Laboratoire setLaboNom(String laboNom) {
		LaboNom = laboNom;
		return this;
	}
	public String getLaboAdresse() {
		return LaboAdresse;
	}
	public Laboratoire setLaboAdresse(String laboAdresse) {
		LaboAdresse = laboAdresse;
		return this;
	}
	public String getLaboVille() {
		return LaboVille;
	}
	public Laboratoire setLaboVille(String laboVille) {
		LaboVille = laboVille;
		return this;
	}
	
	
	
	

}

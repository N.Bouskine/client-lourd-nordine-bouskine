package Entities;

public class Rencontre {
	
	private String EmployeId;
	private String MedecinId;
	private String DateRencontre;
	private String HeureRencontre;
	
	public String getEmployeId() {
		return EmployeId;
	}
	public Rencontre setEmployeId(String employeId) {
		EmployeId = employeId;
		return this;
	}
	
	public String getMedecinId() {
		return MedecinId;
	}
	public Rencontre setMedecinId(String medecinId) {
		MedecinId = medecinId;
		return this;
	}
	
	public String getDateRencontre() {
		return DateRencontre;
	}
	public Rencontre setDateRencontre(String dateRencontre) {
		DateRencontre = dateRencontre;
		return this;
	}
	public String getHeureRencontre() {
		return HeureRencontre;
	}
	public Rencontre setHeureRencontre(String heureRencontre) {
		HeureRencontre = heureRencontre;
		return this;
	}

}

package Entities;

public class Medecin {
	
	
	private String MedecinId;
	private String MedecinNom;
	private String MedecinPrenom;
	private String MedecinMail;
	private String MedecinNumTel;
	private String MedecinAdresse;
	
	public String getMedecinId() {
		return MedecinId;
	}
	public Medecin setMedecinId(String medecinId) {
		MedecinId = medecinId;
		return this;
	}
	public String getMedecinNom() {
		return MedecinNom;
	}
	public Medecin setMedecinNom(String medecinNom) {
		MedecinNom = medecinNom;
		return this;
	}
	public String getMedecinPrenom() {
		return MedecinPrenom;
	}
	public Medecin setMedecinPrenom(String medecinPrenom) {
		MedecinPrenom = medecinPrenom;
		return this;
	}
	public String getMedecinMail() {
		return MedecinMail;
	}
	public Medecin setMedecinMail(String medecinMail) {
		MedecinMail = medecinMail;
		return this;
	}
	public String getMedecinNumTel() {
		return MedecinNumTel;
	}
	public Medecin setMedecinNumTel(String medecinNumTel) {
		MedecinNumTel = medecinNumTel;
		return this;
	}
	public String getMedecinAdresse() {
		return MedecinAdresse;
	}
	public Medecin setMedecinAdresse(String medecinAdresse) {
		MedecinAdresse = medecinAdresse;
		return this;
	}
	
	
	
	

}

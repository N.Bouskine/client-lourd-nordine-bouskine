import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.Commercial;
import Entities.Medecin;
import Entities.Rencontre;
import Entities.TableauRencontre;

public class DAORencontre {
	
private Connection con;
	
	public DAORencontre(Connection con) {
		this.con=con;
	}
	
	public List<TableauRencontre> findAll() throws SQLException {
		
		
        String SQL = "Select employeNom, medecinNom, dateRencontre, heureRencontre from rencontre r JOIN employe e ON r.employeId=e.employeID JOIN medecin m ON r.medecinId=m.medecinId";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        
        
        List<TableauRencontre> liste = new ArrayList<TableauRencontre>();
        TableauRencontre m=null;
        while(rs.next()) {
            m = new TableauRencontre();
            m.setEmployeNom(rs.getString(1)).setMedecinNom(rs.getString(2)).setDateRencontre(rs.getString(3)).setHeureRencontre(rs.getString(4));
            liste.add(m);
        }
        
        
       
        
        
        
        return liste;
    }
	

}

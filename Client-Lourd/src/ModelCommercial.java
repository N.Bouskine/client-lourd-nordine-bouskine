import javax.swing.table.DefaultTableModel;

import java.util.HashSet;
import java.util.List;
import Entities.*;

public class ModelCommercial extends DefaultTableModel{
	
	List<Commercial> liste;
	String[] colNames = {"Identfiant","Nom","Prenom","Mail","NumTel","Laboratoire"};
	HashSet<Commercial> modified = new HashSet<>();
	
	
	public HashSet<Commercial> getModified() {
		return modified;
	}

	public void setModified(HashSet<Commercial> modified) {
		this.modified = modified;
	}

	public ModelCommercial(List<Entities.Commercial> commerciaux) {
		liste = commerciaux;
		
		
	}
	
	@Override
	public int getRowCount() {
		return liste == null ? 0 : liste.size();
	}

	@Override
	public int getColumnCount() {
		return 6;
	}

	@Override
	public String getColumnName(int column) {
		return colNames[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column !=0);
	}

	@Override
	public Object getValueAt(int row, int column) {
		Object value =null;
		Commercial c = liste.get(row);
		switch (column) {
		case 0 : 
			value = c.getEmployeId();
			break;
		case 1 :
			value = c.getEmployeNom();
			break;
		case 2 :
			value = c.getEmployePrenom();
			break;
		case 3 :
			value = c.getEmployeMail();
			break;
		case 4 :
			value = c.getEmployeNumTel();
			break;
		case 5 :
			value = c.getLaboId();
			break;
		}
		
		
		return value;
		
		
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		// TODO Auto-generated method stub
		Commercial c = liste.get(row);
		
		modified.add(c);
	}

	@Override
	public Class getColumnClass(int column)
    {
        for (int row = 0; row < getRowCount(); row++)
        {
            Object o = getValueAt(row, column);

            if (o != null)
            {
                return o.getClass();
            }
        }

        return Object.class;
    }
	
	

}

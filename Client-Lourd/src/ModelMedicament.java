import javax.swing.table.DefaultTableModel;

import java.util.HashSet;
import java.util.List;
import Entities.*;

public class ModelMedicament extends DefaultTableModel{
	
	List<Medicament> liste;
	String[] colNames = {"Identfiant","Nom","Description"};
	HashSet<Medicament> modified = new HashSet<>();
	
	
	public HashSet<Medicament> getModified() {
		return modified;
	}

	public void setModified(HashSet<Medicament> modified) {
		this.modified = modified;
	}

	public ModelMedicament(List<Entities.Medicament> medicaments) {
		liste = medicaments;
		
		
	}
	
	@Override
	public int getRowCount() {
		return liste == null ? 0 : liste.size();
	}

	@Override
	public int getColumnCount() {
		return 3;
	}

	@Override
	public String getColumnName(int column) {
		return colNames[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column !=0);
	}

	@Override
	public Object getValueAt(int row, int column) {
		Object value =null;
		Medicament m = liste.get(row);
		switch (column) {
		case 0 : 
			value = m.getMedicamentId();
			break;
		case 1 :
			value = m.getMedicamentNom();
			break;
		case 2 :
			value = m.getMedicamentDescription();
			break;
		
		}
		
		
		return value;
		
		
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		// TODO Auto-generated method stub
		Medicament m = liste.get(row);
		modified.add(m);
		
		if (column == 0)
        {
            m.setMedicamentId(aValue.toString());
        }
        else
         
        if (column == 1)
        {
        	m.setMedicamentNom(aValue.toString());
        }
        else
         
        if (column == 2)
        {
        	m.setMedicamentDescription(aValue.toString());
        }
        
         
       
	}

	@Override
	public Class getColumnClass(int column)
    {
        for (int row = 0; row < getRowCount(); row++)
        {
            Object o = getValueAt(row, column);

            if (o != null)
            {
                return o.getClass();
            }
        }

        return Object.class;
    }
	
	
	
	

}

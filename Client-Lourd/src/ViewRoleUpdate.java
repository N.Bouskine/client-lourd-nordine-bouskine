
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.Window.Type;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpringLayout;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class ViewRoleUpdate extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JLabel lblNewLabel_1;
	private JComboBox comboBox;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewLaboratoireUpdate frame = new ViewLaboratoireUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewRoleUpdate() {
		setBounds(100, 100, 482, 619);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.BLACK);
		
		JLabel lblNewLabel = new JLabel("Modifier le r\u00F4le de l'utilisateur");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setForeground(Color.WHITE);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JLabel lblNewLabel_2 = new JLabel("Utilisateur");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_3 = new JLabel("Role de l'utilisateur");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBackground(Color.WHITE);
		textField.setColumns(10);
		
		btnNewButton = new JButton("Modifier");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		btnNewButton_1 = new JButton("Annuler");
		btnNewButton_1.setBackground(Color.RED);
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		SpringLayout springLayout = new SpringLayout();
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_3, 14, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_3, -271, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_3, -388, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 317, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 210, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, -6, SpringLayout.WEST, btnNewButton_1);
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_1, 0, SpringLayout.NORTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, 0, SpringLayout.SOUTH, btnNewButton);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton_1, 418, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 421, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -59, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, textField, 38, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, textField, 210, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField, 427, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 41, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_2, 19, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_2, 192, SpringLayout.WEST, panel_1);
		panel_1.setLayout(springLayout);
		panel_1.add(btnNewButton);
		panel_1.add(btnNewButton_1);
		panel_1.add(lblNewLabel_3);
		panel_1.add(lblNewLabel_2);
		panel_1.add(textField);
		
		comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 10));
		comboBox.setForeground(new Color(0, 0, 0));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"reader", "creator", "editor", "admin"}));
		springLayout.putConstraint(SpringLayout.NORTH, comboBox, -1, SpringLayout.NORTH, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.WEST, comboBox, 23, SpringLayout.EAST, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.EAST, comboBox, 0, SpringLayout.EAST, textField);
		panel_1.add(comboBox);
		contentPane.setLayout(gl_contentPane);
	}
	public JTextField getTextField() {
		return textField;
	}
	
	
	
	public JButton getBtnNewButton() {
		return btnNewButton;
	}
	public JButton getBtnNewButton_1() {
		return btnNewButton_1;
	}
	
	public JLabel getLblNewLabel_1() {
		return lblNewLabel_1;
	}
	
public void Afficher(View v) throws  SQLException {
		
		
		
	int i = v.getTable_5().getSelectedRow();
	DefaultTableModel model =  (DefaultTableModel) v.getTable_5().getModel();
	
	lblNewLabel_1.setText(model.getValueAt(i,0).toString());
	textField.setText(model.getValueAt(i,0).toString());
	comboBox.setSelectedItem(model.getValueAt(i,1).toString());
		
		
		
		
		
	}
	
	
	public JComboBox getComboBox() {
		return comboBox;
	}
}

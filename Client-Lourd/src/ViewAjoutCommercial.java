
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Window.Type;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpringLayout;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class ViewAjoutCommercial extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNewButton;
	private JPanel panel_2;
	private JLabel lblNewLabel_5;
	private JTextField textField_2;
	private JTextField textField_4;
	private JLabel lblNewLabel_7;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewLaboratoireUpdate frame = new ViewLaboratoireUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewAjoutCommercial() {
		setBounds(100, 100, 480, 491);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.BLACK);
		
		JLabel lblNewLabel = new JLabel("Ajoutant un nouveau commercial");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setForeground(Color.WHITE);
		
		panel_2 = new JPanel();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 492, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 492, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(442, Short.MAX_VALUE)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 401, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(239, Short.MAX_VALUE))
		);
		
		JLabel lblNewLabel_2 = new JLabel("Identifiant de l'employ\u00E9");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_3 = new JLabel("Nom de l'employ\u00E9");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_4 = new JLabel("Prenom de l'employ\u00E9");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		textField = new JTextField();
		textField.setBackground(Color.WHITE);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		
		btnNewButton = new JButton("Ajouter");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		SpringLayout springLayout = new SpringLayout();
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_2, 14, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_2, -23, SpringLayout.WEST, textField);
		springLayout.putConstraint(SpringLayout.NORTH, textField, 0, SpringLayout.NORTH, lblNewLabel_2);
		springLayout.putConstraint(SpringLayout.WEST, textField, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField, 0, SpringLayout.EAST, textField_1);
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_2, -39, SpringLayout.NORTH, lblNewLabel_4);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_4, 0, SpringLayout.WEST, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.WEST, textField_1, 210, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_1, -65, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_3, 14, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_3, -23, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 234, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, -31, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, textField_1, 0, SpringLayout.NORTH, lblNewLabel_3);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_3, 21, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 322, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, -40, SpringLayout.SOUTH, panel_1);
		panel_1.setLayout(springLayout);
		panel_1.add(btnNewButton);
		panel_1.add(lblNewLabel_4);
		panel_1.add(lblNewLabel_3);
		panel_1.add(lblNewLabel_2);
		panel_1.add(textField_1);
		panel_1.add(textField);
		
		lblNewLabel_5 = new JLabel("T\u00E9l\u00E9phone de l'employ\u00E9");
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_5, 14, SpringLayout.WEST, panel_1);
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel_1.add(lblNewLabel_5);
		
		textField_2 = new JTextField();
		springLayout.putConstraint(SpringLayout.WEST, textField_2, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_2, 0, SpringLayout.EAST, textField_1);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		textField_4 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_2, 103, SpringLayout.SOUTH, textField_4);
		springLayout.putConstraint(SpringLayout.NORTH, textField_4, 0, SpringLayout.NORTH, lblNewLabel_4);
		springLayout.putConstraint(SpringLayout.WEST, textField_4, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_4, 0, SpringLayout.EAST, textField_1);
		panel_1.add(textField_4);
		textField_4.setColumns(10);
		
		lblNewLabel_7 = new JLabel("Mail de l'employ\u00E9");
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_4, -45, SpringLayout.NORTH, lblNewLabel_7);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_7, 14, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_7, -189, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_5, 48, SpringLayout.SOUTH, lblNewLabel_7);
		lblNewLabel_7.setForeground(Color.WHITE);
		lblNewLabel_7.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel_1.add(lblNewLabel_7);
		
		textField_5 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, textField_5, 42, SpringLayout.SOUTH, textField_4);
		springLayout.putConstraint(SpringLayout.WEST, textField_5, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_5, 0, SpringLayout.EAST, textField_1);
		textField_5.setColumns(10);
		panel_1.add(textField_5);
		contentPane.setLayout(gl_contentPane);
	}
	public JTextField getTextField() {
		return textField;
	}
	public JTextField getTextField_1() {
		return textField_1;
	}
	
	
	public JButton getBtnNewButton() {
		return btnNewButton;
	}
	
	
	
	
	
	public JTextField getTextField_2() {
		return textField_2;
	}
	
	public JTextField getTextField_5() {
		return textField_5;
	}
	public JTextField getTextField_4() {
		return textField_4;
	}
}

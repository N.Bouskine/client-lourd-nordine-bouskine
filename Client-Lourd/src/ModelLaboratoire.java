import javax.swing.table.DefaultTableModel;

import java.util.HashSet;
import java.util.List;
import Entities.*;

public class ModelLaboratoire extends DefaultTableModel{
	
	List<Laboratoire> liste;
	String[] colNames = {"Identfiant","Nom","Adresse","Ville"};
	HashSet<Laboratoire> modified = new HashSet<>();
	
	
	public HashSet<Laboratoire> getModified() {
		return modified;
	}

	public void setModified(HashSet<Laboratoire> modified) {
		this.modified = modified;
	}

	public ModelLaboratoire(List<Entities.Laboratoire> laboratoires) {
		liste = laboratoires;
		
		
	}
	
	@Override
	public int getRowCount() {
		return liste == null ? 0 : liste.size();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int column) {
		return colNames[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column !=0);
	}

	@Override
	public Object getValueAt(int row, int column) {
		Object value =null;
		Laboratoire l = liste.get(row);
		switch (column) {
		case 0 : 
			value = l.getLaboCode();
			break;
		case 1 :
			value = l.getLaboNom();
			break;
		case 2 :
			value = l.getLaboAdresse();
			break;
		case 3 :
			value = l.getLaboVille();
			break;
		}
		
		
		return value;
		
		
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		// TODO Auto-generated method stub
		Laboratoire l = liste.get(row);
		
		modified.add(l);
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> clazz=null;
		switch(columnIndex) {
		case 0:
		case 1:
		case 2: {
			clazz= String.class; 
			break; 
			}
		case 3: {
			clazz = Integer.class; 
			break; 
			}
			
		}
		
		return clazz;
	}
	
	

}


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import java.awt.Font;
import java.sql.SQLException;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Window.Type;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewCommercial extends JFrame {

	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JButton btnNewButton;
	private JTextArea textArea;
	private JTextArea textArea_1;
	private JTextArea textArea_2;
	private JTextArea textArea_3;
	private JTextArea txtrLemployeAppartientAu;
	private JTextArea textArea_4;
	private JButton btnModifierCetEmploye;
	private JButton btnNewButton_1;
	private JButton btnNewButton_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewLaboratoire frame = new ViewLaboratoire();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewCommercial() {
		setBounds(100, 100, 485, 455);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		scrollPane = new JScrollPane();
		
		panel = new JPanel();
		panel.setBackground(Color.BLACK);
		scrollPane.setColumnHeaderView(panel);
		
		lblNewLabel = new JLabel("Employ\u00E9e ");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 19));
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 19));
		panel.add(lblNewLabel_1);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 452, GroupLayout.PREFERRED_SIZE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 402, GroupLayout.PREFERRED_SIZE)
		);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		scrollPane.setViewportView(panel_1);
		
		JTextArea txtrIlSeTrouve = new JTextArea();
		txtrIlSeTrouve.setForeground(Color.WHITE);
		txtrIlSeTrouve.setBackground(Color.BLACK);
		txtrIlSeTrouve.setText("Son num\u00E9ro de \r\nt\u00E9l\u00E9phone est :");
		
		JTextArea txtrIlSeSitue = new JTextArea();
		txtrIlSeSitue.setBackground(Color.BLACK);
		txtrIlSeSitue.setForeground(Color.WHITE);
		txtrIlSeSitue.setText("Son adresse mail est :");
		
		textArea_1 = new JTextArea();
		textArea_1.setForeground(Color.WHITE);
		textArea_1.setBackground(Color.BLACK);
		
		JTextArea txtrLeLaboratoireSe = new JTextArea();
		txtrLeLaboratoireSe.setBackground(Color.BLACK);
		txtrLeLaboratoireSe.setForeground(Color.WHITE);
		txtrLeLaboratoireSe.setText("L'employ\u00E9e se nomme ");
		
		textArea = new JTextArea();
		textArea.setForeground(Color.WHITE);
		textArea.setBackground(Color.BLACK);
		
		btnNewButton = new JButton("Supprimer cet employ\u00E9e");
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setBackground(new Color(255, 0, 0));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		textArea_2 = new JTextArea();
		textArea_2.setForeground(Color.WHITE);
		textArea_2.setBackground(Color.BLACK);
		
		textArea_3 = new JTextArea();
		textArea_3.setForeground(Color.WHITE);
		textArea_3.setBackground(Color.BLACK);
		
		txtrLemployeAppartientAu = new JTextArea();
		txtrLemployeAppartientAu.setBackground(Color.BLACK);
		txtrLemployeAppartientAu.setForeground(Color.WHITE);
		txtrLemployeAppartientAu.setText("L'employ\u00E9e appartient \r\nau laboratoire :");
		
		textArea_4 = new JTextArea();
		textArea_4.setForeground(Color.WHITE);
		textArea_4.setBackground(Color.BLACK);
		
		btnModifierCetEmploye = new JButton("Modifier cet employ\u00E9e");
		btnModifierCetEmploye.setForeground(new Color(255, 255, 255));
		btnModifierCetEmploye.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnModifierCetEmploye.setBackground(new Color(0, 0, 128));
		
		btnNewButton_1 = new JButton("Voir");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		btnNewButton_2 = new JButton("Poser un rendez-vous");
		btnNewButton_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(31)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel_1.createSequentialGroup()
											.addComponent(txtrLeLaboratoireSe, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE)
											.addGap(17)
											.addComponent(textArea_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addComponent(txtrLemployeAppartientAu, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
										.addComponent(txtrIlSeTrouve, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel_1.createSequentialGroup()
											.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
												.addComponent(textArea_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(textArea, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(btnNewButton_1))
										.addComponent(textArea_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addComponent(txtrIlSeSitue, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(textArea_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED, 76, Short.MAX_VALUE))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(169)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(btnNewButton, GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
								.addComponent(btnModifierCetEmploye, GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
								.addComponent(btnNewButton_2, GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE))))
					.addContainerGap(110, GroupLayout.PREFERRED_SIZE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(21)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addComponent(txtrLeLaboratoireSe, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
									.addComponent(textArea, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addComponent(textArea_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addGap(18)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtrIlSeSitue, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textArea_2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addComponent(txtrIlSeTrouve, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtrLemployeAppartientAu, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(textArea_4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 13, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(110)
							.addComponent(textArea_3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addPreferredGap(ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
					.addComponent(btnNewButton_2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnModifierCetEmploye)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
					.addGap(68))
		);
		panel_1.setLayout(gl_panel_1);
		contentPane.setLayout(gl_contentPane);
	}

	public JLabel getLblNewLabel() {
		return lblNewLabel;
	}
	public JLabel getLblNewLabel_1() {
		return lblNewLabel_1;
	}
	
	
	
public void Afficher(View v) throws  SQLException {
		
		
		
		lblNewLabel_1.setText(v.getTextField_15().getText().toString());
		
		textArea_3.setText(v.getTextField_14().getText().toString());
		
		textArea.setText(v.getTextField_12().getText().toString());
		
		textArea_1.setText(v.getTextField_11().getText().toString());
		
		textArea_2.setText(v.getTextField_13().getText().toString());
		
		textArea_4.setText(v.getComboBox_1().getSelectedItem().toString());
		
		
		
	}
	
	
	
	public JButton getBtnNewButton() {
		return btnNewButton;
	}
	public JTextArea getTextArea() {
		return textArea;
	}
	public JTextArea getTextArea_1() {
		return textArea_1;
	}
	public JTextArea getTextArea_2() {
		return textArea_2;
	}
	public JTextArea getTextArea_3() {
		return textArea_3;
	}
	public JTextArea getTextArea_4() {
		return textArea_4;
	}
	public JButton getBtnModifierCetEmploye() {
		return btnModifierCetEmploye;
	}
	public JButton getBtnNewButton_1() {
		return btnNewButton_1;
	}
	public JButton getBtnNewButton_2() {
		return btnNewButton_2;
	}
}

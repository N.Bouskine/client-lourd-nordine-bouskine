import javax.swing.table.DefaultTableModel;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import Entities.*;

public class ModelRencontre extends DefaultTableModel{
	
	List<TableauRencontre> liste;
	String[] colNames = {"Nom de l'employ�","Nom du m�decin","Date du rendez-vous","Heure de rendez-vous"};
	HashSet<TableauRencontre> modified = new HashSet<>();
	
	
	public HashSet<TableauRencontre> getModified() {
		return modified;
	}

	public void setModified(HashSet<TableauRencontre> modified) {
		this.modified = modified;
	}

	public ModelRencontre(List<TableauRencontre> rencontres) {
		liste = rencontres;
		
		
	}
	
	@Override
	public int getRowCount() {
		return liste == null ? 0 : liste.size();
	}

	@Override
	public int getColumnCount() {
		return 4;
	}

	@Override
	public String getColumnName(int column) {
		return colNames[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column !=0);
	}

	@Override
	public Object getValueAt(int row, int column) {
		Object value =null;
		TableauRencontre l = liste.get(row);
		switch (column) {
		case 0 : 
			value = l.getEmployeNom();
			break;
		case 1 :
			value = l.getMedecinNom();
			break;
		case 2 :
			value = l.getDateRencontre();
			break;
		case 3 :
			value = l.getHeureRencontre();
			break;
		
		}
		
		
		return value;
		
		
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		// TODO Auto-generated method stub
		TableauRencontre l = liste.get(row);
		
		modified.add(l);
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		Class<?> clazz=null;
		switch(columnIndex) {
		case 0:
		case 1:
		case 2: {
			clazz= String.class; 
			break; 
			}
		case 3: {
			clazz = Integer.class; 
			break; 
			}
			
		}
		
		return clazz;
	}
	
	

}

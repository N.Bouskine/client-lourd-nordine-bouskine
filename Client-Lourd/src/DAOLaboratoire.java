
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import Entities.Commercial;
import Entities.Contient;
import Entities.Laboratoire;
import Entities.Medicament;

public class DAOLaboratoire {
	
	private Connection con;
	
	public DAOLaboratoire(Connection con) {
		this.con=con;
	}
	
	public List<Laboratoire> findAll() throws SQLException {
        String SQL = "Select * from laboratoire";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        List<Laboratoire> laboratoires = new ArrayList<Laboratoire>();
        Laboratoire l = null;
        while(rs.next()) {
            l = new Laboratoire();
            l.setLaboCode(rs.getString(1)).setLaboNom(rs.getString(2)).setLaboAdresse(rs.getString(3)).setLaboVille(rs.getString(4));
            laboratoires.add(l);
        }
        
        
        return laboratoires;
    }
	
	public void save(View v) throws SQLException {
    	
    	String SQL="insert laboratoire(laboId,laboNom,laboAdresse,laboVille) values (?,?,?,?)";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	ps.setString(1, v.getTextField_2().getText());
    	ps.setString(2, v.getTextField().getText());
    	ps.setString(3, v.getTextField_1().getText());
    	ps.setString(4, v.getComboBox().getSelectedItem().toString());
    	
    	ps.executeQuery();
    	
    }
	
	
	public void update(ViewLaboratoireUpdate viewLU, View v) throws SQLException {
	    	
	    	String SQL="UPDATE laboratoire SET laboId = ?, laboNom = ?, laboAdresse = ?, laboVille = ? where laboId=?";
	    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
	    	
	    	ps.setString(1, viewLU.getTextField().getText().toString());
	    	ps.setString(2, viewLU.getTextField_1().getText().toString());
	    	ps.setString(3, viewLU.getTextField_2().getText().toString());
	    	ps.setString(4, viewLU.getComboBox().getSelectedItem().toString());
	    	ps.setString(5, v.getTextField_2().getText().toString());
	    	
	    	ps.executeQuery();
	    	
	    }
	
	public void delete(View v) throws SQLException {
		
		
		String SQL="DELETE FROM laboratoire WHERE laboId = ?";
    	PreparedStatement ps = con.prepareStatement(SQL);
    	ps.setString(1,v.getTextField_2().getText());
    	
    	ps.executeQuery();
		
	}
	
	public List<Laboratoire> findbyname(String texteRecherche) throws SQLException {
        String SQL = "Select * from laboratoire where laboId LIKE '" + texteRecherche + "%' or laboNom LIKE '" + texteRecherche + "%' or laboAdresse LIKE '" + texteRecherche + "%' or laboVille LIKE '" + texteRecherche + "%'";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        
        List<Laboratoire> laboratoires = new ArrayList<Laboratoire>();
        Laboratoire l = null;
        while(rs.next()) {
            l = new Laboratoire();
            l.setLaboCode(rs.getString(1)).setLaboNom(rs.getString(2)).setLaboAdresse(rs.getString(3)).setLaboVille(rs.getString(4));
            laboratoires.add(l);
        }
        
        return laboratoires;
    }
	
	public List<Commercial> findEmployee(View v) throws SQLException {
        String SQL = "Select * from employe where laboID = ?";
        PreparedStatement ps = con.prepareStatement(SQL);
        ps.setString(1, v.getTextField_2().getText().toString());
        
        ResultSet rs = ps.executeQuery();
        
        
        List<Commercial> commerciaux = new ArrayList<Commercial>();
        Commercial c = null;
        while(rs.next()) {
            c = new Commercial();
            c.setEmployeId(rs.getString(1)).setEmployeNom(rs.getString(2)).setEmployePrenom(rs.getString(3)).setEmployeMail(rs.getString(4)).setEmployeNumTel(rs.getString(5)).setLaboId(rs.getString(6));
            commerciaux.add(c);
        }
        
        
        return commerciaux;
	}
	
	public List<Commercial> findEmployee2(ViewLaboratoire vl) throws SQLException {
        String SQL = "Select * from employe where laboID = ?";
        PreparedStatement ps = con.prepareStatement(SQL);
        ps.setString(1, vl.getLblNewLabel_1().getText().toString());
        
        ResultSet rs = ps.executeQuery();
        
        
        List<Commercial> commerciaux = new ArrayList<Commercial>();
        Commercial c = null;
        while(rs.next()) {
            c = new Commercial();
            c.setEmployeId(rs.getString(1)).setEmployeNom(rs.getString(2)).setEmployePrenom(rs.getString(3)).setEmployeMail(rs.getString(4)).setEmployeNumTel(rs.getString(5)).setLaboId(rs.getString(6));
            commerciaux.add(c);
        }
        
        
        return commerciaux;
	}
	
	public List<Medicament> findMedicament(View v) throws SQLException {
        String SQL = "Select * from contient where laboId = ?";
        PreparedStatement ps = con.prepareStatement(SQL);
        ps.setString(1, v.getTextField_2().getText().toString());
        
        ResultSet rs = ps.executeQuery();
        
        List<Medicament> medicaments = new ArrayList<Medicament>();
        Medicament m = null;
        while(rs.next()) {
        	
        	String SQL2 = "Select * from medicament where medicamentId = ?";
            PreparedStatement ps2 = con.prepareStatement(SQL2);
            ps2.setString(1, rs.getString(2));
            
            ResultSet rs2 = ps2.executeQuery();
            
            
            while(rs2.next()) {
            	m = new Medicament();
                m.setMedicamentId(rs2.getString(1)).setMedicamentNom(rs2.getString(2)).setMedicamentDescription(rs2.getString(3));
                medicaments.add(m);
            }
        }
        
        
        return medicaments;
	}
	
	public List<Medicament> findMedicament2(ViewLaboratoire vL) throws SQLException {
        String SQL = "Select * from contient where laboId = ?";
        PreparedStatement ps = con.prepareStatement(SQL);
        ps.setString(1, vL.getLblNewLabel_1().getText().toString());
        
        ResultSet rs = ps.executeQuery();
        
        List<Medicament> medicaments = new ArrayList<Medicament>();
        Medicament m = null;
        while(rs.next()) {
        	
        	String SQL2 = "Select * from medicament where medicamentId = ?";
            PreparedStatement ps2 = con.prepareStatement(SQL2);
            ps2.setString(1, rs.getString(2));
            
            ResultSet rs2 = ps2.executeQuery();
            
            
            while(rs2.next()) {
            	m = new Medicament();
                m.setMedicamentId(rs2.getString(1)).setMedicamentNom(rs2.getString(2)).setMedicamentDescription(rs2.getString(3));
                medicaments.add(m);
            }
        }
        
        
        return medicaments;
	}
	
	public List<Contient> afficheGestionStock(ViewGestionStock viewGS) throws SQLException {
    	
    	String SQL="Select * from contient where laboId=? and medicamentId=?";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	
    	
    	ps.setString(1, viewGS.getTextField().getText().toString());
    	ps.setString(2, viewGS.getTextField_1().getText().toString());
    	
    	
    	ResultSet rs = ps.executeQuery();
    	List<Contient> contients = new ArrayList<Contient>();
    	Contient c = null;
        while(rs.next()) {
            c = new Contient();
            c.setLaboId(rs.getString(1)).setMedicamentId(rs.getString(2)).setQuantite(rs.getString(3));
            contients.add(c);
        }
        
        
        return contients;
    	
    }
	
	public void AjoutNouveauMedicament(ViewAjoutMedicament v, ViewLaboratoire vl) throws SQLException {
    	
		String SQL="insert medicament(medicamentId,medicamentNom,medicamentDescription) values (?,?,?)";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	ps.setString(1, v.getTextField().getText().toString());
    	ps.setString(2, v.getTextField_1().getText().toString());
    	ps.setString(3, v.getTextArea().getText().toString());
    	
    	
    	
    	ps.executeQuery();
		
    	String SQL2="insert contient(laboId,medicamentId,Quantite) values (?,?,?)";
    	PreparedStatement ps2 = (PreparedStatement) con.prepareStatement(SQL2);
    	ps2.setString(1, vl.getLblNewLabel_1().getText().toString());
    	ps2.setString(2, v.getTextField().getText().toString());
    	ps2.setString(3, v.getTextField_2().getText().toString());
    	
    	
    	ps2.executeQuery();
    	
    }
	public void AjoutMedicamentExistant(ViewAjoutMedicament v, ViewLaboratoire vl) throws SQLException {
		String SQL="Select * from medicament where medicamentNom = ?";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	
    	
    	ps.setString(1, v.getComboBox().getSelectedItem().toString());
    	
    	
    	
    	ResultSet rs = ps.executeQuery();
    	List<Medicament> medicaments = new ArrayList<Medicament>();
        Medicament m=null;
        while(rs.next()) {
        	m = new Medicament();
            m.setMedicamentId(rs.getString(1)).setMedicamentNom(rs.getString(2)).setMedicamentDescription(rs.getString(3));
            medicaments.add(m);
        }
    	
    	String SQL2="insert contient(laboId,medicamentId,Quantite) values (?,?,?)";
    	PreparedStatement ps2 = (PreparedStatement) con.prepareStatement(SQL2);
    	ps2.setString(1, vl.getLblNewLabel_1().getText().toString());
    	ps2.setString(2, medicaments.get(0).getMedicamentId());
    	ps2.setString(3, v.getTextField_3().getText().toString());
    	
    	
    	ps2.executeQuery();
    	
    }

	
	public void gestionStock(ViewGestionStock viewGS) throws SQLException {
    	
    	String SQL="UPDATE contient SET Quantite = ? where laboId=? and medicamentId=?";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	
    	ps.setString(1, viewGS.getTextField_5().getText().toString());
    	ps.setString(2, viewGS.getTextField().getText().toString());
    	ps.setString(3, viewGS.getTextField_1().getText().toString());
    	
    	
    	ps.executeQuery();
    	
    }
	
	public void deleteMedicament(ViewGestionStock viewGS) throws SQLException {
		
		
		String SQL="DELETE FROM contient where laboId = ? and medicamentId=?";
    	PreparedStatement ps = con.prepareStatement(SQL);
    	ps.setString(1, viewGS.getTextField().getText().toString());
    	ps.setString(2, viewGS.getTextField_1().getText().toString());
    	
    	ps.executeQuery();
		
	}
	
	public void comboBoxMedicament(ViewAjoutMedicament v) throws SQLException
    {
        
        String SQL = " Select * from medicament";
        PreparedStatement ps = con.prepareStatement(SQL);
        ResultSet rs = ps.executeQuery();
        v.getComboBox().addItem("");
        while (rs.next())
        {
        	
            v.getComboBox().addItem(rs.getString(2));
        }
		try {
			ps = con.prepareStatement(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
        
        
    }
	public void AjoutNouveauCommercial(ViewAjoutCommercial v, ViewLaboratoire vl) throws SQLException {
    	
		String SQL="insert employe(employeID,employeNom,employePrenom,employeMail,employeNumTel,laboID) values (?,?,?,?,?,?)";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	ps.setString(1, v.getTextField().getText());
    	ps.setString(2, v.getTextField_1().getText());
    	ps.setString(3, v.getTextField_4().getText());
    	ps.setString(4, v.getTextField_5().getText());
    	ps.setString(5, v.getTextField_2().getText());
    	ps.setString(6, vl.getLblNewLabel_1().getText());
    	
    	
    	ps.executeQuery();
    	
    }
	
	public void updateCommercial(ViewLaboCommercialUpdate viewCU, ViewCommercial v) throws SQLException {
    	
		String SQL="UPDATE employe SET employeID = ?, employeNom = ?, employePrenom = ?, employeMail = ?, employeNumTel = ?, laboID = ? where employeID=?";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	
    	ps.setString(1, viewCU.getTextField().getText().toString());
    	ps.setString(2, viewCU.getTextField_1().getText().toString());
    	ps.setString(3, viewCU.getTextField_2().getText().toString());
    	ps.setString(4, viewCU.getTextField_3().getText().toString());
    	ps.setString(5, viewCU.getTextField_4().getText().toString());
    	ps.setString(6, viewCU.getComboBox().getSelectedItem().toString());
    	ps.setString(7, v.getLblNewLabel_1().getText().toString());
	    	
	    	ps.executeQuery();
	    	
	    }
	
	public void deleteCommercial(ViewCommercial v) throws SQLException {
		
		
		String SQL="DELETE FROM employe WHERE employeId = ?";
    	PreparedStatement ps = con.prepareStatement(SQL);
    	ps.setString(1,v.getLblNewLabel_1().getText());
    	
    	ps.executeQuery();
		
	}
	
	
	
	
	
	
	
	

}

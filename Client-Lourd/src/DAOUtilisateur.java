import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.Role;
import Entities.Utilisateur;



public class DAOUtilisateur {
	
private Connection con;
	
	public DAOUtilisateur(Connection con) {
		this.con=con;
	}
	
	public List<Utilisateur> findAll() throws SQLException {
		
		
        String SQL = "Select utilisateur, roleNom from authentifcation a JOIN roles r ON a.roleId=r.roleId";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        
        
        List<Utilisateur> liste = new ArrayList<Utilisateur>();
        Utilisateur m=null;
        while(rs.next()) {
            m = new Utilisateur();
            m.setUtilisateur(rs.getString(1)).setRoleNom(rs.getString(2));
            liste.add(m);
        }
        
        
       
        
        
        
        return liste;
    }
	
	public void update(ViewRoleUpdate viewRU) throws SQLException {
    	
		String SQL2="select * from roles where roleNom=?";
    	PreparedStatement ps2 = (PreparedStatement) con.prepareStatement(SQL2);
    	ps2.setString(1, viewRU.getComboBox().getSelectedItem().toString());
		
    	ResultSet rs2 = ps2.executeQuery();
    	
    	List<Role> liste = new ArrayList<Role>();
        Role m=null;
        while(rs2.next()) {
            m = new Role();
            m.setRoleId(rs2.getString(1)).setRoleNom(rs2.getString(2));
            liste.add(m);
        }
		
    	String SQL="UPDATE authentifcation SET  roleId = ? where utilisateur=?";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	
    	ps.setString(1, liste.get(0).getRoleId());
    	ps.setString(2, viewRU.getTextField().getText().toString());
    	
    	
    	ps.executeQuery();
    	
    }

}
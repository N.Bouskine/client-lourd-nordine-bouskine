
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import Entities.Laboratoire;
import Entities.Medecin;

public class DAOMedecin {
	
	private Connection con;
	
	public DAOMedecin(Connection con) {
		this.con=con;
	}
	
	public List<Medecin> findAll() throws SQLException {
        String SQL = "Select * from medecin";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        List<Medecin> medecins = new ArrayList<Medecin>();
        Medecin m=null;
        while(rs.next()) {
        	m = new Medecin();
            m.setMedecinId(rs.getString(1)).setMedecinNom(rs.getString(2)).setMedecinPrenom(rs.getString(3)).setMedecinMail(rs.getString(4)).setMedecinNumTel(rs.getString(5)).setMedecinAdresse(rs.getString(6));
            medecins.add(m);
        }
        
        
        return medecins;
        
    }
	
	public void save(View v) throws SQLException {
    	
    	String SQL="insert medecin(medecinId,medecinNom,medecinPrenom,medecinMail,medecinNumTel,medecinAdresse) values (?,?,?,?,?,?)";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	ps.setString(1, v.getTextField_9().getText());
    	ps.setString(2, v.getTextField_4().getText());
    	ps.setString(3, v.getTextField_5().getText());
    	ps.setString(4, v.getTextField_6().getText());
    	ps.setString(5, v.getTextField_7().getText());
    	ps.setString(6, v.getTextField_8().getText());
    	
    	
    	ps.executeQuery();
    	
    }
	
	
	public void update(ViewMedecinUpdate viewMU, View v) throws SQLException {
	    	
	    	String SQL="UPDATE medecin SET medecinId = ?, medecinNom = ?, medecinPrenom = ?, medecinMail = ?, medecinNumTel = ?, medecinAdresse = ? where medecinId=?";
	    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
	    	
	    	ps.setString(1, viewMU.getTextField().getText().toString());
	    	ps.setString(2, viewMU.getTextField_1().getText().toString());
	    	ps.setString(3, viewMU.getTextField_2().getText().toString());
	    	ps.setString(4, viewMU.getTextField_3().getText().toString());
	    	ps.setString(5, viewMU.getTextField_4().getText().toString());
	    	ps.setString(6, viewMU.getTextField_5().getText().toString());
	    	ps.setString(7, v.getTextField_9().getText().toString());
	    	
	    	ps.executeQuery();
	    	
	    }
	
	public void delete(View v) throws SQLException {
		
		
		String SQL="DELETE FROM medecin WHERE medecinId = ?";
    	PreparedStatement ps = con.prepareStatement(SQL);
    	ps.setString(1,v.getTextField_9().getText());
    	
    	ps.executeQuery();
		
	}
	
	public List<Medecin> findbyname(String texteRecherche) throws SQLException {
        String SQL = "Select * from medecin where medecinId LIKE '" + texteRecherche + "%' or medecinNom LIKE '" + texteRecherche + "%' or medecinPrenom LIKE '" + texteRecherche + "%' or medecinMail LIKE '" + texteRecherche + "%' or medecinNumTel LIKE '" + texteRecherche + "%' or medecinAdresse LIKE '" + texteRecherche + "%'";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        
        List<Medecin> medecins = new ArrayList<Medecin>();
        Medecin m = null;
        while(rs.next()) {
            m = new Medecin();
            m.setMedecinId(rs.getString(1)).setMedecinNom(rs.getString(2)).setMedecinPrenom(rs.getString(3)).setMedecinMail(rs.getString(4)).setMedecinNumTel(rs.getString(5)).setMedecinAdresse(rs.getString(6));
            medecins.add(m);
        }
        
        return medecins;
    }
	
	
	
	
	
	
	
	

}

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import com.mysql.cj.jdbc.Driver;

import Entities.Commercial;
import Entities.Contient;
import Entities.Laboratoire;
import Entities.Medecin;
import Entities.Medicament;
import Entities.Rencontre;
import Entities.TableauRencontre;
import Entities.Utilisateur;






public class Controller {
	
	
	private View view;
	private Singleton singleton;
	private Laboratoire laboratoire;
	private ViewLaboratoire viewL;
	private ViewLaboratoireUpdate viewLU;
	DAOLaboratoire daolaboratoire;
	List<Laboratoire> laboratoires;
	ModelLaboratoire md;
	private Medecin medecin;
	private ViewMedecin viewM;
	private ViewMedecinUpdate viewMU;
	DAOMedecin daomedecin;
	ArrayList medecins;
	ModelMedecin mc;
	DAOCommercial daocommercial;
	ViewCommercial viewC;
	ViewCommercialUpdate viewCU;
	private Medicament medicament;
	private ViewMedicament viewD;
	private ViewMedicamentUpdate viewDU;
	DAOMedicament daomedicament;
	ArrayList medicaments;
	ModelMedicament mmd;
	private ViewGestionStock viewGS;
	ArrayList contients;
	private ViewAjoutMedicament viewAM;
	private ViewAjoutCommercial viewAC;
	private ViewLaboCommercialUpdate viewLCU;
	private ViewRencontre viewR;
	DAORencontre daorencontre;
	private String user;
	private String password;
	ArrayList utilisateurs;
	DAOUtilisateur daoutilisateur;
	ModelUtilisateur mdu;
	private ViewRoleUpdate viewRU;
	private ViewRendezVous viewRDV;
	
	
	
	
	public Controller(Connexion co ,View v, DAOLaboratoire daop, DAOMedecin daom, DAOCommercial daoc,DAOMedicament daod, DAOConnection daocx, DAORencontre daor, DAOUtilisateur daou) throws SQLException {
		
		
		
		
		
		
		view=v;
		ViewLaboratoire viewL =  new ViewLaboratoire();
		ViewLaboratoireUpdate viewLU =  new ViewLaboratoireUpdate();
		daolaboratoire=daop;
		
		ViewCommercial viewC =  new ViewCommercial();
		ViewCommercialUpdate viewCU =  new ViewCommercialUpdate();
		daocommercial = daoc;
		
		ViewMedicament viewD =  new ViewMedicament();
		ViewMedicamentUpdate viewDU =  new ViewMedicamentUpdate();
		
		
		List<Laboratoire> laboratoires = daolaboratoire.findAll();
		ModelLaboratoire md = new ModelLaboratoire(laboratoires);
		view.getTable().setModel(md);
		
		
	
		view.getBtnNewButton().addActionListener((ActionListener) new ActionListener() {
					

					@Override
					public void actionPerformed(ActionEvent e) {
						String regex = "^LA"+"(?=.*[0-9])"+"(?=\\S+$).{4,4}$";
						Pattern p = Pattern.compile(regex);
						Matcher m = p.matcher(view.getTextField_2().getText().toString());
						System.out.println(m.matches());
						
						
								if(Boolean.compare(m.matches(),true)==0) {
									try {
										
											doJobAjouter();
											
										} catch (SQLException e1) {
											e1.printStackTrace();
										}
								}else {
									JOptionPane.showMessageDialog(null, "L'identifiant du laboratoire doit �tre du type LA0001");
								}
								
						
					}
				});
		
		view.getTable().addMouseListener((MouseListener) new MouseListener() {
			

		

			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					doJobClicker();
					view.getBtnNewButton_1().setVisible(true);
					view.getBtnNewButton_2().setVisible(true);
					view.getBtnNewButton_3().setVisible(true);
					view.getBtnNewButton_4().setVisible(true);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		view.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					viewLU.Afficher(view);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				viewLU.setVisible(true);
				
			}
		});
		
		view.getBtnNewButton_2().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							int n = JOptionPane.showConfirmDialog(null,
									"Voulez-vous vraiment supprimer ce laboratoire","Confirm Dialog",JOptionPane.YES_NO_OPTION);
							if(n == JOptionPane.YES_OPTION)
							{
							doJobDelete();
							}
							
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
					}
				});
		
		view.getBtnNewButton_3().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				view.getTextField().setText ("") ; 
				view.getTextField_1().setText ("") ;
				view.getTextField_2().setText ("") ;
				view.getTextField_2().setEditable(true) ;
				view.getTextField().setEditable(true) ;
				view.getTextField_1().setEditable(true) ;
				view.getComboBox().setEnabled(true);
				view.getComboBox().setSelectedItem ("") ;
				view.getBtnNewButton_1().setVisible(false);
				view.getBtnNewButton_2().setVisible(false);
				view.getBtnNewButton_3().setVisible(false);
				view.getBtnNewButton_4().setVisible(false);
				
			}
		});
		
		view.getBtnNewButton_4().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							viewL.Afficher(view);
							List<Commercial> commerciaux1 = daolaboratoire.findEmployee(view);
							
							ModelCommercial mdc = new ModelCommercial(commerciaux1);
							viewL.getTable().setModel(mdc);
							List<Medicament> medicaments1 = daolaboratoire.findMedicament(view);
							
							ModelMedicament mmc = new ModelMedicament(medicaments1);
							viewL.getTable_1().setModel(mmc);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						viewL.setVisible(true);
						
					}
				});
		
		viewL.getTable().addMouseListener((MouseListener) new MouseListener() {
			

			

			@Override
			public void mouseClicked(MouseEvent e) {
				
				
				viewC.setVisible(true);
				int i = viewL.getTable().getSelectedRow();
				DefaultTableModel model =  (DefaultTableModel) viewL.getTable().getModel();
				
				viewC.getLblNewLabel_1().setText(model.getValueAt(i,0).toString());
				
				viewC.getTextArea_3().setText(model.getValueAt(i,4).toString());
				
				viewC.getTextArea().setText(model.getValueAt(i,1).toString());
				
				viewC.getTextArea_1().setText(model.getValueAt(i,2).toString());
				
				viewC.getTextArea_2().setText(model.getValueAt(i,3).toString());
				
				viewC.getTextArea_4().setText(model.getValueAt(i,5).toString());
				
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		view.getBtnNewButton_5().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				doJob_chercher();
				
				
			}
		});
		
		viewL.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				view.setVisible(true);
				
			}
		});
		
		viewLU.getBtnNewButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						
						try {
							int n = JOptionPane.showConfirmDialog(null,
									"Voulez-vous vraiment modifier ce laboratoire","Confirm Dialog",JOptionPane.YES_NO_OPTION);
							if(n == JOptionPane.YES_OPTION)
							{
								doJobUpdate(viewLU, view);
								viewLU.setVisible(false);
							}
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				});
		
		viewLU.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				viewLU.setVisible(false);
				view.getTextField().setText ("") ; 
				view.getTextField_1().setText ("") ;
				view.getTextField_2().setText ("") ;
				view.getTextField_2().setEditable(true) ;
				view.getTextField().setEditable(true) ;
				view.getTextField_1().setEditable(true) ;
				view.getComboBox().setEnabled(true);
				view.getComboBox().setSelectedItem ("") ;
				view.getBtnNewButton_1().setVisible(false);
				view.getBtnNewButton_2().setVisible(false);
				view.getBtnNewButton_3().setVisible(false);
				view.getBtnNewButton_4().setVisible(false);
				
			}
		});
		
		
		
		
		view=v;
		ViewMedecin viewM =  new ViewMedecin();
		ViewMedecinUpdate viewMU =  new ViewMedecinUpdate();
		daomedecin=daom;
		
		List<Medecin> medecins = daomedecin.findAll();
		ModelMedecin mc = new ModelMedecin(medecins);
		view.getTable_1().setModel(mc);
		
        
		
		
	
		view.getBtnNewButton_6().addActionListener((ActionListener) new ActionListener() {
					

					@Override
					public void actionPerformed(ActionEvent e) {
						try {
							doJobAjouter1();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
					}
				});
		
		view.getTable_1().addMouseListener((MouseListener) new MouseListener() {
			

		

			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					doJobClicker1();
					view.getBtnNewButton_10().setVisible(true);
					view.getBtnNewButton_7().setVisible(true);
					view.getBtnNewButton_8().setVisible(true);
					view.getBtnNewButton_9().setVisible(true);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		view.getBtnNewButton_7().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					viewMU.Afficher(view);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				viewMU.setVisible(true);
				
			}
		});
		
		view.getBtnNewButton_8().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							int n = JOptionPane.showConfirmDialog(null,
									"Voulez-vous vraiment supprimer ce m�decin","Confirm Dialog",JOptionPane.YES_NO_OPTION);
							if(n == JOptionPane.YES_OPTION)
							{
							doJobDelete1();
							}
							
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
					}
				});
		
		view.getBtnNewButton_10().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				view.getTextField_4().setText ("") ; 
				view.getTextField_5().setText ("") ;
				view.getTextField_6().setText ("") ;
				view.getTextField_7().setText ("") ;
				view.getTextField_8().setText ("") ;
				view.getTextField_9().setText ("") ;
				view.getTextField_9().setEditable(true) ;
				view.getTextField_4().setEditable(true) ;
				view.getTextField_5().setEditable(true) ;
				view.getTextField_6().setEditable(true) ;
				view.getTextField_7().setEditable(true) ;
				view.getTextField_8().setEditable(true) ;
				view.getTextField_9().setEditable(true) ;
				
				view.getBtnNewButton_10().setVisible(false);
				view.getBtnNewButton_7().setVisible(false);
				view.getBtnNewButton_8().setVisible(false);
				view.getBtnNewButton_9().setVisible(false);
				
			}
		});
		
		view.getBtnNewButton_9().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							viewM.Afficher(view);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						viewM.setVisible(true);
						
					}
				});
		
		view.getBtnNewButton_11().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				doJob_chercher1();
				
				
			}
		});
		
		viewM.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				view.setVisible(true);
				
			}
		});
		
		viewMU.getBtnNewButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						
						try {
							int n = JOptionPane.showConfirmDialog(null,
									"Voulez-vous vraiment modifier ce m�decin","Confirm Dialog",JOptionPane.YES_NO_OPTION);
							if(n == JOptionPane.YES_OPTION)
							{
								doJobUpdate1(viewMU, view);
								viewMU.setVisible(false);
							}
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				});
		
		viewMU.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				viewMU.setVisible(false);
				view.getTextField_4().setText ("") ; 
				view.getTextField_5().setText ("") ;
				view.getTextField_6().setText ("") ;
				view.getTextField_7().setText ("") ;
				view.getTextField_8().setText ("") ;
				view.getTextField_9().setEditable(true) ;
				view.getTextField_4().setEditable(true) ;
				view.getTextField_5().setEditable(true) ;
				view.getTextField_6().setEditable(true) ;
				view.getTextField_7().setEditable(true) ;
				view.getTextField_8().setEditable(true) ;
				
				view.getBtnNewButton_10().setVisible(false);
				view.getBtnNewButton_7().setVisible(false);
				view.getBtnNewButton_8().setVisible(false);
				view.getBtnNewButton_9().setVisible(false);
				
			}
		});
		
		
		daomedicament = daod;
		List<Medicament> medicaments = daomedicament.findAll();
		ModelMedicament mmd = new ModelMedicament(medicaments);
		view.getTable_3().setModel(mmd);
		
		view.getBtnNewButton_18().addActionListener((ActionListener) new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					doJobAjouter3();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		});

		view.getTable_3().addMouseListener((MouseListener) new MouseListener() {
			
		
		
		
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					doJobClicker3();
					view.getBtnNewButton_19().setVisible(true);
					view.getBtnNewButton_20().setVisible(true);
					view.getBtnNewButton_21().setVisible(true);
					view.getBtnNewButton_22().setVisible(true);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		
			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}
		
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		view.getBtnNewButton_19().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					viewDU.Afficher(view);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				viewDU.setVisible(true);
				
				
			}
		});
		
		view.getBtnNewButton_20().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							int n = JOptionPane.showConfirmDialog(null,
									"Voulez-vous vraiment supprimer ce m�dicament","Confirm Dialog",JOptionPane.YES_NO_OPTION);
							if(n == JOptionPane.YES_OPTION)
							{
							doJobDelete3();
							}
							
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
					}
				});
		
		view.getBtnNewButton_22().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				view.getTextField_16().setText ("") ; 
				view.getTextField_18().setText ("") ;
				view.getTextArea().setText ("") ;
				
				view.getTextField_16().setEditable(true) ;
				view.getTextField_18().setEditable(true) ;
				view.getTextArea().setEditable(true) ;
				
				
				view.getBtnNewButton_19().setVisible(false);
				view.getBtnNewButton_20().setVisible(false);
				view.getBtnNewButton_21().setVisible(false);
				view.getBtnNewButton_22().setVisible(false);
			}
		});
		
		view.getBtnNewButton_21().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							viewD.Afficher(view);
							List<Laboratoire> laboratoire1 = daomedicament.findLaboratoire(view);
							
							ModelLaboratoire mdl = new ModelLaboratoire(laboratoire1);
							viewD.getTable().setModel(mdl);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						viewD.setVisible(true);
						
					}
				});
		
		view.getBtnNewButton_23().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					doJob_chercher3();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
		});
		
		viewD.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				view.setVisible(true);
				
			}
		});
		
		viewD.getTable().addMouseListener((MouseListener) new MouseListener() {
					
		
					
		
					@Override
					public void mouseClicked(MouseEvent e) {
						
						
						viewL.setVisible(true);
						int i = viewD.getTable().getSelectedRow();
						DefaultTableModel model =  (DefaultTableModel) viewD.getTable().getModel();
						
						viewL.getLblNewLabel_1().setText(model.getValueAt(i,0).toString());
						
						viewL.getTextArea().setText(model.getValueAt(i,1).toString());
						
						viewL.getTextArea_1().setText(model.getValueAt(i,1).toString());
						
						viewL.getTextArea_2().setText(model.getValueAt(i,3).toString());
						
						List<Commercial> commerciaux1 = new ArrayList<Commercial>();
						
						try {
							commerciaux1 = daolaboratoire.findEmployee2(viewL);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						
						ModelCommercial mdc = new ModelCommercial(commerciaux1);
						viewL.getTable().setModel(mdc);
						List<Medicament> medicaments1= new ArrayList<Medicament>();
						try {
							medicaments1 = daolaboratoire.findMedicament2(viewL);
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						ModelMedicament mmc = new ModelMedicament(medicaments1);
						viewL.getTable_1().setModel(mmc);
						
						
					}
		
					@Override
					public void mousePressed(MouseEvent e) {
						
						
					}
		
					@Override
					public void mouseReleased(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
		
					@Override
					public void mouseEntered(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
		
					@Override
					public void mouseExited(MouseEvent e) {
						// TODO Auto-generated method stub
						
					}
				});
		
		view.getBtnNewButton_5().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				doJob_chercher();
				
				
			}
		});
		
		viewDU.getBtnNewButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						
						try {
							int n = JOptionPane.showConfirmDialog(null,
									"Voulez-vous vraiment modifier ce m�dicament","Confirm Dialog",JOptionPane.YES_NO_OPTION);
							if(n == JOptionPane.YES_OPTION)
							{
								doJobUpdate3(viewDU, view);
								viewDU.setVisible(false);
							}
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				});
		
		viewDU.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				viewDU.setVisible(false);
				view.getTextField_16().setText ("") ; 
				view.getTextField_18().setText ("") ;
				view.getTextArea().setText ("") ;
				
				view.getTextField_16().setEditable(true) ;
				view.getTextField_18().setEditable(true) ;
				view.getTextArea().setEditable(true) ;
				
				
				view.getBtnNewButton_19().setVisible(false);
				view.getBtnNewButton_20().setVisible(false);
				view.getBtnNewButton_21().setVisible(false);
				view.getBtnNewButton_22().setVisible(false);
			}
		});
		
		
		daoc.comboBoxLaboId(v);
		
		List<Commercial> commerciaux = daocommercial.findAll();
		ModelCommercial mc1 = new ModelCommercial(commerciaux);
		view.getTable_2().setModel(mc1);
		
		view.getBtnNewButton_12().addActionListener((ActionListener) new ActionListener() {
			

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					doJobAjouter2();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		});

		view.getTable_2().addMouseListener((MouseListener) new MouseListener() {
			
		
		
		
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					doJobClicker2();
					view.getBtnNewButton_13().setVisible(true);
					view.getBtnNewButton_14().setVisible(true);
					view.getBtnNewButton_15().setVisible(true);
					view.getBtnNewButton_16().setVisible(true);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		
			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}
		
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		view.getBtnNewButton_13().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					viewCU.Afficher(view);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				viewCU.setVisible(true);
				
				
			}
		});
		
		view.getBtnNewButton_14().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							int n = JOptionPane.showConfirmDialog(null,
									"Voulez-vous vraiment supprimer cet employ�e","Confirm Dialog",JOptionPane.YES_NO_OPTION);
							if(n == JOptionPane.YES_OPTION)
							{
							doJobDelete2();
							}
							
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
						
					}
				});
		
		view.getBtnNewButton_16().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				view.getTextField_11().setText ("") ; 
				view.getTextField_12().setText ("") ;
				view.getTextField_13().setText ("") ;
				view.getTextField_14().setText ("") ;
				view.getTextField_15().setText ("") ;
				view.getComboBox_1().setSelectedItem ("") ;
				view.getTextField_11().setEditable(true) ;
				view.getTextField_12().setEditable(true) ;
				view.getTextField_13().setEditable(true) ;
				view.getTextField_14().setEditable(true) ;
				view.getTextField_15().setEditable(true) ;
				view.getComboBox_1().setEnabled(true) ;
				
				view.getBtnNewButton_13().setVisible(false);
				view.getBtnNewButton_14().setVisible(false);
				view.getBtnNewButton_15().setVisible(false);
				view.getBtnNewButton_16().setVisible(false);
			}
		});
		
		view.getBtnNewButton_15().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							viewC.Afficher(view);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						viewC.setVisible(true);
						
					}
				});
		
		view.getBtnNewButton_17().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				doJob_chercher2();
				
				
			}
		});
		
		viewC.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				view.setVisible(true);
				
			}
		});
		
		viewCU.getBtnNewButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						
						try {
							int n = JOptionPane.showConfirmDialog(null,
									"Voulez-vous vraiment modifier cet employ�e","Confirm Dialog",JOptionPane.YES_NO_OPTION);
							if(n == JOptionPane.YES_OPTION)
							{
								doJobUpdate2(viewCU, view);
								viewCU.setVisible(false);
							}
							
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				});
		
		viewCU.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				viewCU.setVisible(false);
				view.getTextField_11().setText ("") ; 
				view.getTextField_12().setText ("") ;
				view.getTextField_13().setText ("") ;
				view.getTextField_14().setText ("") ;
				view.getTextField_15().setText ("") ;
				view.getComboBox_1().setSelectedItem ("") ;
				view.getTextField_11().setEditable(true) ;
				view.getTextField_12().setEditable(true) ;
				view.getTextField_13().setEditable(true) ;
				view.getTextField_14().setEditable(true) ;
				view.getTextField_15().setEditable(true) ;
				view.getComboBox_1().setEnabled(true) ;
				
				view.getBtnNewButton_13().setVisible(false);
				view.getBtnNewButton_14().setVisible(false);
				view.getBtnNewButton_15().setVisible(false);
				view.getBtnNewButton_16().setVisible(false);
			}
		});
		
		ViewGestionStock viewGS = new ViewGestionStock();
		
		viewL.getTable_1().addMouseListener((MouseListener) new MouseListener() {
			

			

			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					
					
					viewGS.Afficher(viewL);
					viewGS.setVisible(true);
					List<Contient> contients = daolaboratoire.afficheGestionStock(viewGS);
					viewGS.getTextField_5().setText(contients.get(0).getQuantite().toString());
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		viewGS.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
					int n = JOptionPane.showConfirmDialog(null,
							"Voulez-vous vraiment modifier le stock","Confirm Dialog",JOptionPane.YES_NO_OPTION);
					if(n == JOptionPane.YES_OPTION)
					{
						daolaboratoire.gestionStock(viewGS);
						
						JOptionPane.showMessageDialog(null, "Stock modifi� avec succ�s !");	
						
						List<Medicament> medicaments1 = daolaboratoire.findMedicament2(viewL);
						ModelMedicament mmd1 = new ModelMedicament(medicaments1);
						viewL.getTable_1().setModel(mmd1);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		viewGS.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					int n = JOptionPane.showConfirmDialog(null,
							"Voulez-vous vraiment supprimer cet m�dicament pour ce laboratoire","Confirm Dialog",JOptionPane.YES_NO_OPTION);
					if(n == JOptionPane.YES_OPTION)
					{
						daolaboratoire.deleteMedicament(viewGS);
						
						JOptionPane.showMessageDialog(null, "M�dicament supprim� avec succ�s !");
						List<Medicament> medicaments1 = daolaboratoire.findMedicament2(viewL);
						ModelMedicament mmd1 = new ModelMedicament(medicaments1);
						viewL.getTable_1().setModel(mmd1);
					}
					
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		
		ViewAjoutMedicament viewAM =  new ViewAjoutMedicament();
		ViewAjoutCommercial viewAC =  new ViewAjoutCommercial();
		
		viewL.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				viewAM.setVisible(true);
				try {
					daolaboratoire.comboBoxMedicament(viewAM);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		viewL.getBtnNewButton_2().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				viewAC.setVisible(true);
				
			}
		});
		
		viewAM.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					daolaboratoire.AjoutNouveauMedicament(viewAM, viewL);
					List<Medicament> medicaments1 = daolaboratoire.findMedicament2(viewL);
					ModelMedicament mmd1 = new ModelMedicament(medicaments1);
					viewL.getTable_1().setModel(mmd1);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "Medicament ajout� avec succ�s !");
				
			}
		});

		viewAM.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					daolaboratoire.AjoutMedicamentExistant(viewAM, viewL);
					List<Medicament> medicaments1 = daolaboratoire.findMedicament2(viewL);
					ModelMedicament mmd1 = new ModelMedicament(medicaments1);
					viewL.getTable_1().setModel(mmd1);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "Medicament ajout� avec succ�s !");
				
			}
		});
		
		viewAC.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					daolaboratoire.AjoutNouveauCommercial(viewAC, viewL);
					List<Commercial> commerciaux2 = daolaboratoire.findEmployee2(viewL);
					ModelCommercial mmc1 = new ModelCommercial(commerciaux2);
					viewL.getTable().setModel(mmc1);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "Medicament ajout� avec succ�s !");
				
			}
		});
		
		ViewLaboCommercialUpdate viewLCU =  new ViewLaboCommercialUpdate();
		
		viewC.getBtnModifierCetEmploye().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
					viewLCU.Afficher2(viewC, view);
					viewLCU.getComboBox().setEnabled(false);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				viewLCU.setVisible(true);
					
				
				
			}
		});
		
		viewC.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					int n = JOptionPane.showConfirmDialog(null,
							"Voulez-vous vraiment supprimer cet employ� pour ce laboratoire","Confirm Dialog",JOptionPane.YES_NO_OPTION);
					if(n == JOptionPane.YES_OPTION)
					{
						daolaboratoire.deleteCommercial(viewC);
						
						JOptionPane.showMessageDialog(null, "Commercial supprim� avec succ�s !");
						List<Commercial> commerciaux3 = daolaboratoire.findEmployee2(viewL);
						ModelCommercial mmc2 = new ModelCommercial(commerciaux3);
						viewL.getTable().setModel(mmc2);
					}
					
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				viewC.setVisible(false);
				
			}
		});
		
		viewLCU.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
					int n = JOptionPane.showConfirmDialog(null,
							"Voulez-vous vraiment modifier cet employ�","Confirm Dialog",JOptionPane.YES_NO_OPTION);
					if(n == JOptionPane.YES_OPTION)
					{
						daolaboratoire.updateCommercial(viewLCU, viewC);
						
						JOptionPane.showMessageDialog(null, "Commercial modifi� avec succ�s !");	
						
						List<Commercial> commerciaux3 = daolaboratoire.findEmployee2(viewL);
						ModelCommercial mmc2 = new ModelCommercial(commerciaux3);
						viewL.getTable().setModel(mmc2);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				viewC.setVisible(false);
				viewLCU.setVisible(false);
				
			}
		});
		
		viewC.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
		
				viewL.setVisible(true);
				List<Laboratoire> liste;
				try {
					liste = daocommercial.findLaboratoire(viewC);
					viewL.getLblNewLabel_1().setText(liste.get(0).getLaboCode());
					
					viewL.getTextArea().setText(liste.get(0).getLaboNom());
					
					viewL.getTextArea_1().setText(liste.get(0).getLaboAdresse());
					
					viewL.getTextArea_2().setText(liste.get(0).getLaboVille());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				List<Commercial> commerciaux1 = new ArrayList<Commercial>();
				
				try {
					commerciaux1 = daolaboratoire.findEmployee2(viewL);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				ModelCommercial mdc = new ModelCommercial(commerciaux1);
				viewL.getTable().setModel(mdc);
				List<Medicament> medicaments1= new ArrayList<Medicament>();
				try {
					medicaments1 = daolaboratoire.findMedicament2(viewL);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				ModelMedicament mmc = new ModelMedicament(medicaments1);
				viewL.getTable_1().setModel(mmc);
				
			}
			
		});
		
		ViewRencontre viewR = new ViewRencontre();
		
		viewC.getBtnNewButton_2().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					viewR.Afficher(viewC);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				viewR.setVisible(true);
				
				}
			});
		
		
		viewR.getTglbtnNewToggleButton().addItemListener((ItemListener) new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					viewR.getLblNewLabel_1_3_1().setVisible(true);
					viewR.getComboBox().setVisible(true);
					viewR.getLblNewLabel_1_3().setVisible(false);
					viewR.getLblNewLabel_1_4().setVisible(false);
					viewR.getLblNewLabel_1_5().setVisible(false);
					viewR.getTextField_3().setVisible(false);
					viewR.getTextField_4().setVisible(false);
					viewR.getTextField_5().setVisible(false);
					viewR.getLblNewLabel_1_6().setVisible(false);
					viewR.getLblNewLabel_1_7().setVisible(false);
					viewR.getLblNewLabel_1_8().setVisible(false);
					viewR.getTextField_6().setVisible(false);
					viewR.getTextField_7().setVisible(false);
					viewR.getTextField_8().setVisible(false);
					viewR.getBtnNewButton_1().setVisible(true);
					viewR.getBtnNewButton().setVisible(false);
				}else {
					viewR.getLblNewLabel_1_3_1().setVisible(false);
					viewR.getComboBox().setVisible(false);
					viewR.getLblNewLabel_1_3().setVisible(true);
					viewR.getLblNewLabel_1_4().setVisible(true);
					viewR.getLblNewLabel_1_5().setVisible(true);
					viewR.getTextField_3().setVisible(true);
					viewR.getTextField_4().setVisible(true);
					viewR.getTextField_5().setVisible(true);
					viewR.getLblNewLabel_1_6().setVisible(true);
					viewR.getLblNewLabel_1_7().setVisible(true);
					viewR.getLblNewLabel_1_8().setVisible(true);
					viewR.getTextField_6().setVisible(true);
					viewR.getTextField_7().setVisible(true);
					viewR.getTextField_8().setVisible(true);
					viewR.getBtnNewButton_1().setVisible(false);
					viewR.getBtnNewButton().setVisible(true);
			}}}
        );
		
		viewR.getBtnNewButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						try {
							daocommercial.AjoutNouvelRencontre(viewR);
							JOptionPane.showMessageDialog(null, "Rendez-Vous ajout� avec succ�s !");
							List<TableauRencontre> rencontres = daorencontre.findAll();
							ModelRencontre mdrc = new ModelRencontre(rencontres);
							view.getTable_4().setModel(mdrc);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
						}
					});
		
		viewR.getBtnNewButton_1().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				try {
					daocommercial.AjoutNouvelRencontre2(viewR);
					JOptionPane.showMessageDialog(null, "Rendez-Vous ajout� avec succ�s !");
					List<TableauRencontre> rencontres = daorencontre.findAll();
					ModelRencontre mdrc = new ModelRencontre(rencontres);
					view.getTable_4().setModel(mdrc);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				}
			});
		
		daocommercial.comboBoxRencontre(viewR);
		
		daorencontre = daor;
		
		List<TableauRencontre> rencontres = daorencontre.findAll();
		ModelRencontre mdrc = new ModelRencontre(rencontres);
		view.getTable_4().setModel(mdrc);
		
		daoutilisateur = daou;
		
		List<Utilisateur> utilisateurs = daoutilisateur.findAll();
		ModelUtilisateur mdu = new ModelUtilisateur(utilisateurs);
		view.getTable_5().setModel(mdu);
		
		ViewRoleUpdate viewRU =  new ViewRoleUpdate();
		
		view.getTable_5().addMouseListener((MouseListener) new MouseListener() {
			

			

			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					
					
					viewRU.Afficher(view);
					viewRU.setVisible(true);
					
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		viewRU.getBtnNewButton().addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				try {
					int n = JOptionPane.showConfirmDialog(null,
							"Voulez-vous vraiment modifier ce r�le","Confirm Dialog",JOptionPane.YES_NO_OPTION);
					if(n == JOptionPane.YES_OPTION)
					{
						daou.update(viewRU);
						JOptionPane.showMessageDialog(null, "R�le modifi� avec succ�s !");
						viewRU.setVisible(false);
						List<Utilisateur> utilisateurs = daoutilisateur.findAll();
						ModelUtilisateur mdu = new ModelUtilisateur(utilisateurs);
						view.getTable_5().setModel(mdu);
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		
		ViewRendezVous viewRDV =  new ViewRendezVous();
		
		view.getTable_4().addMouseListener((MouseListener) new MouseListener() {
			

			

			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					
					
					viewRDV.Afficher(view);
					viewRDV.setVisible(true);
					
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		
		
		
		
		//Authentification et gestion de roles 
		
		
		ViewAuthentification a = new ViewAuthentification();
		
		co.getBtnNewButton_1().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				a.setVisible(true);
				co.setVisible(false);
				
			}
			
		});
		
		co.getBtnNewButton().addActionListener(new ActionListener() {

			

			@Override
			public void actionPerformed(ActionEvent e) {
				user = co.getTextField().getText();
				password = co.getPasswordField().getText();
				
				try {
					if((Boolean.compare(daocx.connexion(user,password),true)) ==0) {
						JOptionPane.showMessageDialog(null, "Connection r�ussie ! ");
						v.setVisible(true);
						co.setVisible(false);
						
						if((daocx.role(user)).equals("1")) {
							
							v.getPanel_14().setVisible(false);
							v.getPanel_15().setVisible(false);
							v.getPanel_16().setVisible(false);
							v.getPanel_17().setVisible(false);
							v.getBtnNewButton().setVisible(false);
							v.getBtnNewButton_6().setVisible(false);
							v.getBtnNewButton_12().setVisible(false);
							v.getBtnNewButton_18().setVisible(false);
							viewC.getBtnNewButton().setVisible(false);
							viewC.getBtnNewButton_2().setVisible(false);
							viewC.getBtnModifierCetEmploye().setVisible(false);
							viewGS.getBtnNewButton().setVisible(false);
							viewGS.getBtnNewButton_1().setVisible(false);
							viewL.getBtnNewButton_1().setVisible(false);
							viewL.getBtnNewButton_2().setVisible(false);
							v.getTabbedPane().remove(v.getPanel_18());
						
						
						
					}
						if((daocx.role(user)).equals("2")) {
						
						v.getPanel_14().setVisible(false);
						v.getPanel_15().setVisible(false);
						v.getPanel_16().setVisible(false);
						v.getPanel_17().setVisible(false);
						v.getPanel_18().setVisible(false);
						viewC.getBtnNewButton().setVisible(false);
						viewC.getBtnModifierCetEmploye().setVisible(false);
						viewGS.getBtnNewButton().setVisible(false);
						viewGS.getBtnNewButton_1().setVisible(false);
						v.getTabbedPane().remove(v.getPanel_18());
					
					
				}
						if((daocx.role(user)).equals("3")) {
							v.getTabbedPane().remove(v.getPanel_18());
				}
						
						
						
					}else {
						JOptionPane.showMessageDialog(null, "Cet identifiant ou mot de passe est incorrect !");
					}
				} catch (HeadlessException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
			
		});
		
		

		
		
		a.getBtnNewButton().addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String regex = "^(?=.*[0-9])"
	                       + "(?=.*[a-z])(?=.*[A-Z])"
	                       + "(?=.*[@#$%^&+=-])"
	                       + "(?=\\S+$).{8,20}$";
				
				Pattern p = Pattern.compile(regex);
				Matcher m = p.matcher(a.getTextField_1().getText().toString());
				
				
				if(Boolean.compare(a.getTextField().getText().toString().equals(""),false)==0 && Boolean.compare(a.getTextField_1().getText().toString().equals(""),false)==0) {
					if (a.getTextField_1().getText().toString().equals(a.getTextField_2().getText().toString())) {
						if(Boolean.compare(m.matches(),true)==0) {
							try {
								daocx.creercompte(a);
								JOptionPane.showMessageDialog(null, "Authentification r�ussie ! ");
								co.setVisible(true);
								a.setVisible(false);
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}else {
							JOptionPane.showMessageDialog(null, "Veillez � ce que les mots de passes correspondent !\r\n"
									+ "Veillez � ce qu'ils comportent au moins :\r\n"
									+ "- 1 majuscule\r\n"
									+ "- 1 miniscule\r\n"
									+ "- 1 symbole\r\n"
									+ "- 1 chiffre\r\n"
									+ "- au moins 8 caract�res.\r\n"
									+ "");
						}
					}else {
						JOptionPane.showMessageDialog(null, "Veillez � ce que les mots de passes correspondent !\r\n"
								+ "Veillez � ce qu'ils comportent au moins :\r\n"
								+ "- 1 majuscule\r\n"
								+ "- 1 miniscule\r\n"
								+ "- 1 symbole\r\n"
								+ "- 1 chiffre\r\n"
								+ "- au moins 8 caract�res.\r\n"
								+ " ");
					}
				
				}else {
					JOptionPane.showMessageDialog(null, "Veillez � ce que les mots de passes correspondent !\r\n"
							+ "Veillez � ce qu'ils comportent au moins :\r\n"
							+ "- 1 majuscule\r\n"
							+ "- 1 miniscule\r\n"
							+ "- 1 symbole\r\n"
							+ "- 1 chiffre\r\n"
							+ "- au moins 8 caract�res.\r\n"
							+ "");
				}
			
			}	
			
		});
	
	}	
	
	
	
	
	private void doJobAjouter() throws SQLException{
			
			
		try {
				
				daolaboratoire.save(view);
				JOptionPane.showMessageDialog(null, "Laboratoire ajout� avec succ�s !");
				List<Laboratoire> laboratoires = daolaboratoire.findAll();
				ModelLaboratoire md = new ModelLaboratoire(laboratoires);
				view.getTable().setModel(md);
				
				
				
				}catch(Exception e ) {
				JOptionPane.showMessageDialog(null, "Cet identifiant existe d�ja");
			}
		}
	
	private void doJobUpdate(ViewLaboratoireUpdate viewLU, View view) throws SQLException{
		try {
			
			daolaboratoire.update(viewLU, view);
			
			JOptionPane.showMessageDialog(null, "Laboratoire modifi� avec succ�s !");
			
			List<Laboratoire> laboratoires = daolaboratoire.findAll();
			ModelLaboratoire md = new ModelLaboratoire(laboratoires);
			view.getTable().setModel(md);
			
			view.getTextField().setText ("") ; 
			view.getTextField_1().setText ("") ;
			view.getTextField_2().setText ("") ;
			view.getTextField_2().setEditable(true) ;
			view.getTextField().setEditable(true) ;
			view.getTextField_1().setEditable(true) ;
			view.getComboBox().setEnabled(true);
			view.getComboBox().setSelectedItem ("") ;
			view.getBtnNewButton_1().setVisible(false);
			view.getBtnNewButton_2().setVisible(false);
			view.getBtnNewButton_3().setVisible(false);
			view.getBtnNewButton_4().setVisible(false);
			
			
			
			
		}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Ce laboratoire ne peut �tre modifi�");
		}
	}
	
	private void doJobDelete() throws SQLException{
		try {
			
			daolaboratoire.delete(view);
			
			JOptionPane.showMessageDialog(null, "Laboratoire supprim� avec succ�s !");
			
			List<Laboratoire> laboratoires = daolaboratoire.findAll();
			ModelLaboratoire md = new ModelLaboratoire(laboratoires);
			view.getTable().setModel(md);
			
			view.getTextField().setText ("") ; 
			view.getTextField_1().setText ("") ;
			view.getTextField_2().setText ("") ;
			view.getTextField_2().setEditable(true) ;
			view.getTextField().setEditable(true) ;
			view.getTextField_1().setEditable(true) ;
			view.getComboBox().setEnabled(true);
			view.getComboBox().setSelectedItem ("") ;
			view.getBtnNewButton_1().setVisible(false);
			view.getBtnNewButton_2().setVisible(false);
			view.getBtnNewButton_3().setVisible(false);
			view.getBtnNewButton_4().setVisible(false);
			
			
			
			
		}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Ce laboratoire ne peut �tre supprim�");
		}
	}
	
	private void doJob_chercher() {
		try {
			
			laboratoires=daolaboratoire.findbyname(view.getTextField_3().getText());
			ModelLaboratoire cd = new ModelLaboratoire(laboratoires);
			view.getTable().setModel(cd);
			
			
			
		}catch(Exception e){
			System.out.println("RECHERCHER");
			
			
		}
		
	}
	
	
	private void doJobClicker() throws SQLException{
		
		view.clicker();
		List<Laboratoire> laboratoires = daolaboratoire.findAll();
		ModelLaboratoire md = new ModelLaboratoire(laboratoires);
		view.getTable().setModel(md);
		
		
		
	}
	
	private void doJobAjouter1() throws SQLException{
			
			
			try {
				
				daomedecin.save(view);
				JOptionPane.showMessageDialog(null, "M�decin ajout� avec succ�s !");
				ArrayList medecins = (ArrayList) daomedecin.findAll();
				ModelMedecin mdm = new ModelMedecin(medecins);
				view.getTable_1().setModel(mdm);
				
				
				
				}catch(Exception e ) {
				JOptionPane.showMessageDialog(null, "Cet identifiant existe d�ja");
			}
		}
	
	private void doJobUpdate1(ViewMedecinUpdate viewMU, View view) throws SQLException{
		try {
			
			daomedecin.update(viewMU, view);
			
			JOptionPane.showMessageDialog(null, "M�decin modifi� avec succ�s !");
			
			ArrayList medecins = (ArrayList) daomedecin.findAll();
			ModelMedecin mdm = new ModelMedecin(medecins);
			view.getTable_1().setModel(mdm);
			
			view.getTextField_4().setText ("") ; 
			view.getTextField_5().setText ("") ;
			view.getTextField_6().setText ("") ;
			view.getTextField_7().setText ("") ;
			view.getTextField_8().setText ("") ;
			view.getTextField_9().setText ("") ;
			view.getTextField_9().setEditable(true) ;
			view.getTextField_4().setEditable(true) ;
			view.getTextField_5().setEditable(true) ;
			view.getTextField_6().setEditable(true) ;
			view.getTextField_7().setEditable(true) ;
			view.getTextField_8().setEditable(true) ;
			view.getTextField_9().setEditable(true) ;
			
			view.getBtnNewButton_10().setVisible(false);
			view.getBtnNewButton_7().setVisible(false);
			view.getBtnNewButton_8().setVisible(false);
			view.getBtnNewButton_9().setVisible(false);
			
			
			
			
		}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Ce laboratoire ne peut �tre modifi�");
		}
	}
	
	private void doJobDelete1() throws SQLException{
		try {
			
			daomedecin.delete(view);
			
			JOptionPane.showMessageDialog(null, "M�decin supprim� avec succ�s !");
			
			ArrayList medecins = (ArrayList) daomedecin.findAll();
			ModelMedecin mdm = new ModelMedecin(medecins);
			view.getTable_1().setModel(mdm);
			
			view.getTextField_4().setText ("") ; 
			view.getTextField_5().setText ("") ;
			view.getTextField_6().setText ("") ;
			view.getTextField_7().setText ("") ;
			view.getTextField_8().setText ("") ;
			view.getTextField_9().setText ("") ;
			view.getTextField_9().setEditable(true) ;
			view.getTextField_4().setEditable(true) ;
			view.getTextField_5().setEditable(true) ;
			view.getTextField_6().setEditable(true) ;
			view.getTextField_7().setEditable(true) ;
			view.getTextField_8().setEditable(true) ;
			view.getTextField_9().setEditable(true) ;
			
			view.getBtnNewButton_10().setVisible(false);
			view.getBtnNewButton_7().setVisible(false);
			view.getBtnNewButton_8().setVisible(false);
			view.getBtnNewButton_9().setVisible(false);
			
			
			
			
		}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Ce laboratoire ne peut �tre supprim�");
		}
	}
	
	private void doJob_chercher1() {
		try {
			
			ArrayList medecins=(ArrayList) daomedecin.findbyname(view.getTextField_10().getText());
			ModelMedecin mdm = new ModelMedecin(medecins);
			view.getTable_1().setModel(mdm);
			
			
			
			
		}catch(Exception e){
			System.out.println("RECHERCHER");
			
			
		}
		
	}
	
	
	private void doJobClicker1() throws SQLException{
		
		view.clicker1();
		ArrayList medecins = (ArrayList) daomedecin.findAll();
		ModelMedecin mdm = new ModelMedecin(medecins);
		view.getTable_1().setModel(mdm);
		
		
		
	}
	
		private void doJobAjouter2() throws SQLException{
			
			
			try {
				
				daocommercial.save(view);
				JOptionPane.showMessageDialog(null, "Employ�e ajout� avec succ�s !");
				ArrayList commerciaux = (ArrayList) daocommercial.findAll();
				ModelCommercial mc = new ModelCommercial(commerciaux);
				view.getTable_2().setModel(mc);
				
				
				
				}catch(Exception e ) {
				JOptionPane.showMessageDialog(null, "Cet identifiant existe d�ja");
			}
		}
	
	private void doJobUpdate2(ViewCommercialUpdate viewCU, View view) throws SQLException{
		try {
			
			
			daocommercial.update(viewCU, view);
			
			JOptionPane.showMessageDialog(null, "Employ�e modifi� avec succ�s !");
			
			ArrayList commerciaux = (ArrayList) daocommercial.findAll();
			ModelCommercial mc = new ModelCommercial(commerciaux);
			view.getTable_2().setModel(mc);
			
			view.getTextField_11().setText ("") ; 
			view.getTextField_12().setText ("") ;
			view.getTextField_13().setText ("") ;
			view.getTextField_14().setText ("") ;
			view.getTextField_15().setText ("") ;
			view.getComboBox_1().setSelectedItem ("") ;
			view.getTextField_11().setEditable(true) ;
			view.getTextField_12().setEditable(true) ;
			view.getTextField_13().setEditable(true) ;
			view.getTextField_14().setEditable(true) ;
			view.getTextField_15().setEditable(true) ;
			view.getComboBox_1().setEnabled(true) ;
			
			view.getBtnNewButton_13().setVisible(false);
			view.getBtnNewButton_14().setVisible(false);
			view.getBtnNewButton_15().setVisible(false);
			view.getBtnNewButton_16().setVisible(false);
			
			
			
			
		}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Ce laboratoire ne peut �tre modifi�");
		}
	}
	
	private void doJobDelete2() throws SQLException{
		try {
			
			daocommercial.delete(view);
			
			JOptionPane.showMessageDialog(null, "Employ�e supprim� avec succ�s !");
			
			ArrayList commerciaux = (ArrayList) daocommercial.findAll();
			ModelCommercial mc = new ModelCommercial(commerciaux);
			view.getTable_2().setModel(mc);
			
			view.getTextField_11().setText ("") ; 
			view.getTextField_12().setText ("") ;
			view.getTextField_13().setText ("") ;
			view.getTextField_14().setText ("") ;
			view.getTextField_15().setText ("") ;
			view.getComboBox_1().setSelectedItem ("") ;
			view.getTextField_11().setEditable(true) ;
			view.getTextField_12().setEditable(true) ;
			view.getTextField_13().setEditable(true) ;
			view.getTextField_14().setEditable(true) ;
			view.getTextField_15().setEditable(true) ;
			view.getComboBox_1().setEnabled(true) ;;
			
			view.getBtnNewButton_13().setVisible(false);
			view.getBtnNewButton_14().setVisible(false);
			view.getBtnNewButton_15().setVisible(false);
			view.getBtnNewButton_16().setVisible(false);
			
			
			
			
		}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Ce laboratoire ne peut �tre supprim�");
		}
	}
	
	private void doJob_chercher2() {
		try {
			
			ArrayList commerciaux =(ArrayList) daocommercial.findbyname(view.getTextField_17().getText());
			ModelCommercial mc = new ModelCommercial(commerciaux);
			view.getTable_2().setModel(mc);
			
			
			
			
		}catch(Exception e){
			System.out.println("RECHERCHER");
			
			
		}
		
	}
	
	
	private void doJobClicker2() throws SQLException{
		
		view.clicker2();
		
		
		
	}
	
	private void doJobAjouter3() throws SQLException{
		
		
		try {
			
			daomedicament.save(view);
			JOptionPane.showMessageDialog(null, "M�dicament ajout� avec succ�s !");
			ArrayList medicaments = (ArrayList) daomedicament.findAll();
			ModelMedicament mmd = new ModelMedicament(medicaments);
			view.getTable_3().setModel(mmd);
			
			
			
			}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Cet identifiant existe d�ja");
		}
	}

	private void doJobUpdate3(ViewMedicamentUpdate viewDU, View view) throws SQLException{
		try {
			
			
			daomedicament.update(viewDU, view);
			
			JOptionPane.showMessageDialog(null, "M�dicament modifi� avec succ�s !");
			
			ArrayList medicaments = (ArrayList) daomedicament.findAll();
			ModelMedicament mmd = new ModelMedicament(medicaments);
			view.getTable_3().setModel(mmd);
			
			view.getTextField_16().setText ("") ; 
			view.getTextField_18().setText ("") ;
			view.getTextArea().setText ("") ;
			
			view.getTextField_16().setEditable(true) ;
			view.getTextField_18().setEditable(true) ;
			view.getTextArea().setEditable(true) ;
			
			
			view.getBtnNewButton_19().setVisible(false);
			view.getBtnNewButton_20().setVisible(false);
			view.getBtnNewButton_21().setVisible(false);
			view.getBtnNewButton_22().setVisible(false);
			
			
			
			
		}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Ce laboratoire ne peut �tre modifi�");
		}
	}
	
	private void doJobDelete3() throws SQLException{
		try {
			
			daomedicament.delete(view);
			
			JOptionPane.showMessageDialog(null, "M�dicament supprim� avec succ�s !");
			
			ArrayList medicaments = (ArrayList) daomedicament.findAll();
			ModelMedicament mmd = new ModelMedicament(medicaments);
			view.getTable_3().setModel(mmd);
			
			view.getTextField_16().setText ("") ; 
			view.getTextField_18().setText ("") ;
			view.getTextArea().setText ("") ;
			
			view.getTextField_16().setEditable(true) ;
			view.getTextField_18().setEditable(true) ;
			view.getTextArea().setEditable(true) ;
			
			
			view.getBtnNewButton_19().setVisible(false);
			view.getBtnNewButton_20().setVisible(false);
			view.getBtnNewButton_21().setVisible(false);
			view.getBtnNewButton_22().setVisible(false);
			
			
			
			
		}catch(Exception e ) {
			JOptionPane.showMessageDialog(null, "Ce laboratoire ne peut �tre supprim�");
		}
	}
	
	private void doJob_chercher3() throws SQLException {
		try {
			
			ArrayList medicaments =(ArrayList) daomedicament.findbyname(view.getTextField_19().getText());
			ModelMedicament mmd = new ModelMedicament(medicaments);
			view.getTable_3().setModel(mmd);
			
			
			
			
		}catch(Exception e){
			System.out.println("RECHERCHER");
			
			
		}
		
	}
	
	
	private void doJobClicker3() throws SQLException{
		
		view.clicker3();
		
		
		
	}
	
	
	
}
	
	

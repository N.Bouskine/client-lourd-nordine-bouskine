
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;

import Entities.Commercial;
import Entities.Laboratoire;
import Entities.Medecin;
import Entities.Medicament;

public class DAOCommercial {
	
	private Connection con;
	
	public DAOCommercial(Connection con) {
		this.con=con;
	}
	
	public List<Commercial> findAll() throws SQLException {
        String SQL = "Select * from employe";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        List<Commercial> commerciaux = new ArrayList<Commercial>();
        Commercial c = null;
        while(rs.next()) {
            c = new Commercial();
            c.setEmployeId(rs.getString(1)).setEmployeNom(rs.getString(2)).setEmployePrenom(rs.getString(3)).setEmployeMail(rs.getString(4)).setEmployeNumTel(rs.getString(5)).setLaboId(rs.getString(6));
            commerciaux.add(c);
        }
        
        
        return commerciaux;
    }
	
	public void save(View v) throws SQLException {
    	
    	String SQL="insert employe(employeID,employeNom,employePrenom,employeMail,employeNumTel,laboID) values (?,?,?,?,?,?)";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	ps.setString(1, v.getTextField_15().getText());
    	ps.setString(2, v.getTextField_11().getText());
    	ps.setString(3, v.getTextField_12().getText());
    	ps.setString(4, v.getTextField_13().getText());
    	ps.setString(5, v.getTextField_14().getText());
    	ps.setString(6, v.getComboBox_1().getSelectedItem().toString());
    	
    	
    	ps.executeQuery();
    	
    }
	
	
	public void update(ViewCommercialUpdate viewCU, View v) throws SQLException {
	    	
		String SQL="UPDATE employe SET employeID = ?, employeNom = ?, employePrenom = ?, employeMail = ?, employeNumTel = ?, laboID = ? where employeID=?";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	
    	ps.setString(1, viewCU.getTextField().getText().toString());
    	ps.setString(2, viewCU.getTextField_1().getText().toString());
    	ps.setString(3, viewCU.getTextField_2().getText().toString());
    	ps.setString(4, viewCU.getTextField_3().getText().toString());
    	ps.setString(5, viewCU.getTextField_4().getText().toString());
    	ps.setString(6, viewCU.getComboBox().getSelectedItem().toString());
    	ps.setString(7, v.getTextField_15().getText().toString());
	    	
	    	ps.executeQuery();
	    	
	    }
	
	public void delete(View v) throws SQLException {
		
		
		String SQL="DELETE FROM employe WHERE employeId = ?";
    	PreparedStatement ps = con.prepareStatement(SQL);
    	ps.setString(1,v.getTextField_15().getText());
    	
    	ps.executeQuery();
		
	}
	
	public List<Commercial> findbyname(String texteRecherche) throws SQLException {
        String SQL = "Select * from employe where employeId LIKE '" + texteRecherche + "%' or employeNom LIKE '" + texteRecherche + "%' or employePrenom LIKE '" + texteRecherche + "%' or employeMail LIKE '" + texteRecherche + "%' or employeNumTel LIKE '" + texteRecherche + "%' or laboId LIKE '" + texteRecherche + "%'";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        
        List<Commercial> commerciaux = new ArrayList<Commercial>();
        Commercial c = null;
        while(rs.next()) {
            c = new Commercial();
            c.setEmployeId(rs.getString(1)).setEmployeNom(rs.getString(2)).setEmployePrenom(rs.getString(3)).setEmployeMail(rs.getString(4)).setEmployeNumTel(rs.getString(5)).setLaboId(rs.getString(6));
            commerciaux.add(c);
        }
        
        
        return commerciaux;
    }
	
	public void comboBoxLaboId(View v) throws SQLException
    {
        
        String SQL = " Select laboId from laboratoire ";
        PreparedStatement ps = con.prepareStatement(SQL);
        ResultSet rs = ps.executeQuery();
        v.getComboBox_1().addItem("");
        while (rs.next())
        {
        	
            v.getComboBox_1().addItem(rs.getString(1));
        }
		try {
			ps = con.prepareStatement(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
    }
	
	public List<Laboratoire> findLaboratoire(ViewCommercial vl) throws SQLException {
        String SQL = "Select * from laboratoire where laboId=?";
        PreparedStatement ps = con.prepareStatement(SQL);
        ps.setString(1,vl.getTextArea_4().getText());
        
        ResultSet rs = ps.executeQuery();
        
        List<Laboratoire> laboratoires = new ArrayList<Laboratoire>();
        Laboratoire l = null;
        while(rs.next()) {
            l = new Laboratoire();
            l.setLaboCode(rs.getString(1)).setLaboNom(rs.getString(2)).setLaboAdresse(rs.getString(3)).setLaboVille(rs.getString(4));
            laboratoires.add(l);
        }
        
        return laboratoires;
	}
	
	public void AjoutNouvelRencontre(ViewRencontre v) throws SQLException {
	    	
			String SQL="insert medecin(medecinId,medecinNom,medecinPrenom,medecinMail,medecinNumTel,medecinAdresse) values (?,?,?,?,?,?)";
	    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
	    	ps.setString(1, v.getTextField_3().getText());
	    	ps.setString(2, v.getTextField_4().getText());
	    	ps.setString(3, v.getTextField_5().getText());
	    	ps.setString(4, v.getTextField_6().getText());
	    	ps.setString(5, v.getTextField_7().getText());
	    	ps.setString(6, v.getTextField_8().getText());
	    	
	    	
	    	ps.executeQuery();
			
	    	Date date = new Date();
	        date = ((Date) v.getCalendar().getDate());
	    	
	    	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

	        
			String formattedDate = simpleDateFormat.format(date);

	        java.sql.Date date1 = java.sql.Date.valueOf(formattedDate);
			
	    	String SQL2="insert rencontre(employeId,medecinId,dateRencontre,heureRencontre) values (?,?,?,?)";
	    	PreparedStatement ps2 = (PreparedStatement) con.prepareStatement(SQL2);
	    	ps2.setString(1, v.getTextField().getText().toString());
	    	ps2.setString(2, v.getTextField_3().getText());
	    	ps2.setDate(3, date1);
	    	ps2.setString(4, v.getComboBox_1().getSelectedItem().toString());
	    	
	    	
	    	ps2.executeQuery();
	    	
	    }
	
	public void AjoutNouvelRencontre2(ViewRencontre v) throws SQLException {
    	
		String SQL="Select * from medecin where medecinNom = ?";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	
    	
    	ps.setString(1, v.getComboBox().getSelectedItem().toString());
    	
    	
    	
    	ResultSet rs = ps.executeQuery();
    	List<Medecin> medecins = new ArrayList<Medecin>();
        Medecin m=null;
        while(rs.next()) {
        	m = new Medecin();
            m.setMedecinId(rs.getString(1)).setMedecinNom(rs.getString(2)).setMedecinPrenom(rs.getString(3)).setMedecinMail(rs.getString(4)).setMedecinNumTel(rs.getString(5)).setMedecinAdresse(rs.getString(6));
            medecins.add(m);
        }
        
        Date date = new Date();
        date = ((Date) v.getCalendar().getDate());
        
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        
		String formattedDate = simpleDateFormat.format(date);

        java.sql.Date date1 = java.sql.Date.valueOf(formattedDate);
		
    	String SQL2="insert rencontre(employeId,medecinId,dateRencontre,heureRencontre) values (?,?,?,?)";
    	PreparedStatement ps2 = (PreparedStatement) con.prepareStatement(SQL2);
    	ps2.setString(1, v.getTextField().getText().toString());
    	ps2.setString(2, medecins.get(0).getMedecinId());
    	ps2.setDate(3, date1);
    	ps2.setString(4, v.getComboBox_1().getSelectedItem().toString());
    	
    	
    	ps2.executeQuery();
    	
    }
	
	public void comboBoxRencontre(ViewRencontre v) throws SQLException
    {
        
        String SQL = " Select * from medecin";
        PreparedStatement ps = con.prepareStatement(SQL);
        ResultSet rs = ps.executeQuery();
        v.getComboBox().addItem("");
        while (rs.next())
        {
        	
            v.getComboBox().addItem(rs.getString(2));
        }
		try {
			ps = con.prepareStatement(SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
        
        
    }
	
	
	
	
	
	

}

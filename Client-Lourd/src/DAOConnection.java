import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import Entities.Authentification;
import Entities.Commercial;
import Entities.Laboratoire;

public class DAOConnection {
private Connection con;
	
	public DAOConnection(Connection con) {
		this.con=con;
	}
	
	
	
	public static String hasher(String password) {
		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(password.getBytes());
			byte[] resultByteArray = messageDigest.digest();
			StringBuilder sb = new StringBuilder();
			
			for (byte b : resultByteArray) {
				sb.append(String.format("%02x", b));
			}
			
			return sb.toString();
			
		}catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		
		return "";
	}
	
	public boolean connexion(String user, String password) throws SQLException {
        String SQL = "Select * from authentifcation";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        boolean a = false;
        while(rs.next()) {
            if (user.equals(rs.getString(1))) {
            	if(hasher(password).equals(rs.getString(2))) {
            		a=true;
            	}
            }
        }
        return a;
        
	}
	
	public void creercompte(ViewAuthentification a ) throws SQLException {
		
		String mdphash = hasher(a.getTextField_1().getText());
		String SQL="insert authentifcation(utilisateur,password,roleId) values (?,?,?)";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ps.setString(1, a.getTextField().getText());
    	ps.setString(2, mdphash);
    	ps.setString(3, "1");
    	
    	ps.executeQuery();
        
	}
	
	public String role(String user) throws SQLException {
        String SQL = "Select * from authentifcation where utilisateur = ?";
        PreparedStatement ps = con.prepareStatement(SQL);
        ps.setString(1, user);
        
        ResultSet rs = ps.executeQuery();
        
        List<Authentification> laboratoires = new ArrayList<Authentification>();
        Authentification l = null;
        while(rs.next()) {
            l = new Authentification();
            l.setUtilisateur(rs.getString(1)).setPassword(rs.getString(2)).setRoleId(rs.getString(3));
            laboratoires.add(l);
        }
		String role = laboratoires.get(0).getRoleId();
        
        return role;
	}
	
	
	
	
}


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.table.DefaultTableModel;

import Entities.Commercial;
import Entities.Laboratoire;
import Entities.Medecin;
import Entities.Medicament;

public class DAOMedicament {
	
	private Connection con;
	
	public DAOMedicament(Connection con) {
		this.con=con;
	}
	
	public List<Medicament> findAll() throws SQLException {
        String SQL = "Select * from medicament";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        List<Medicament> medicaments = new ArrayList<Medicament>();
        Medicament m=null;
        while(rs.next()) {
        	m = new Medicament();
            m.setMedicamentId(rs.getString(1)).setMedicamentNom(rs.getString(2)).setMedicamentDescription(rs.getString(3));
            medicaments.add(m);
        }
        
        
        return medicaments;
        
    }
	
	public void save(View v) throws SQLException {
    	
    	String SQL="insert medicament(medicamentId,medicamentNom,medicamentDescription) values (?,?,?)";
    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
    	ps.setString(1, v.getTextField_18().getText());
    	ps.setString(2, v.getTextField_16().getText());
    	ps.setString(3, v.getTextArea().getText());
    	
    	
    	
    	ps.executeQuery();
    	
    }
	
	
	public void update(ViewMedicamentUpdate viewDU, View v) throws SQLException {
	    	
	    	String SQL="UPDATE medicament SET medicamentId = ?, medicamentNom = ?, medicamentDescription = ? where medicamentId=?";
	    	PreparedStatement ps = (PreparedStatement) con.prepareStatement(SQL);
	    	
	    	ps.setString(1, viewDU.getTextField().getText().toString());
	    	ps.setString(2, viewDU.getTextField_1().getText().toString());
	    	ps.setString(3, viewDU.getTextArea().getText().toString());
	    	ps.setString(4, v.getTextField_18().getText().toString());
	    	
	    	ps.executeQuery();
	    	
	    }
	
	public void delete(View v) throws SQLException {
		
		
		String SQL="DELETE FROM medicament WHERE medicamentId = ?";
    	PreparedStatement ps = con.prepareStatement(SQL);
    	ps.setString(1,v.getTextField_18().getText());
    	
    	ps.executeQuery();
		
	}
	
	public List<Medicament> findbyname(String texteRecherche) throws SQLException {
        String SQL = "Select * from medicament where medicamentId LIKE '" + texteRecherche + "%' or medicamentNom LIKE '" + texteRecherche + "%' or medicamentDescription LIKE '" + texteRecherche + "%'";
        PreparedStatement ps = con.prepareStatement(SQL);
        
        ResultSet rs = ps.executeQuery();
        
        
        List<Medicament> medicaments = new ArrayList<Medicament>();
        Medicament m=null;
        while(rs.next()) {
        	m = new Medicament();
            m.setMedicamentId(rs.getString(1)).setMedicamentNom(rs.getString(2)).setMedicamentDescription(rs.getString(3));
            medicaments.add(m);
        }
        
        
        return medicaments;
    }
	
	public List<Laboratoire> findLaboratoire(View v) throws SQLException {
        String SQL = "Select * from contient where medicamentId = ?";
        PreparedStatement ps = con.prepareStatement(SQL);
        ps.setString(1, v.getTextField_18().getText().toString());
        
        ResultSet rs = ps.executeQuery();
        
        List<Laboratoire> laboratoires = new ArrayList<Laboratoire>();
        Laboratoire l = null;
        while(rs.next()) {
        	
        	String SQL2 = "Select * from laboratoire where laboId = ?";
            PreparedStatement ps2 = con.prepareStatement(SQL2);
            ps2.setString(1, rs.getString(1));
            
            ResultSet rs2 = ps2.executeQuery();
            while(rs2.next()) {
            	l = new Laboratoire();
            	l.setLaboCode(rs2.getString(1)).setLaboNom(rs2.getString(2)).setLaboAdresse(rs2.getString(3)).setLaboVille(rs2.getString(4));
                laboratoires.add(l);
            }
        }
        
        
        return laboratoires;
	}
	
	
	
	
	
	
	

}

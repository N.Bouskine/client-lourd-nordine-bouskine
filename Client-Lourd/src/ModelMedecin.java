import javax.swing.table.DefaultTableModel;

import java.util.HashSet;
import java.util.List;
import Entities.*;

public class ModelMedecin extends DefaultTableModel{
	
	List<Medecin> liste;
	String[] colNames = {"Identfiant","Nom","Prenom","Mail","Telephone","Adresse"};
	HashSet<Medecin> modified = new HashSet<>();
	
	
	public HashSet<Medecin> getModified() {
		return modified;
	}

	public void setModified(HashSet<Medecin> modified) {
		this.modified = modified;
	}

	public ModelMedecin(List<Entities.Medecin> medecins) {
		liste = medecins;
		
		
	}
	
	@Override
	public int getRowCount() {
		return liste == null ? 0 : liste.size();
	}

	@Override
	public int getColumnCount() {
		return 6;
	}

	@Override
	public String getColumnName(int column) {
		return colNames[column];
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		return (column !=0);
	}

	@Override
	public Object getValueAt(int row, int column) {
		Object value =null;
		Medecin m = liste.get(row);
		switch (column) {
		case 0 : 
			value = m.getMedecinId();
			break;
		case 1 :
			value = m.getMedecinNom();
			break;
		case 2 :
			value = m.getMedecinPrenom();
			break;
		case 3 :
			value = m.getMedecinMail();
			break;
		case 4 :
			value = m.getMedecinNumTel();
			break;
		case 5 :
			value = m.getMedecinAdresse();
			break;
		}
		
		
		return value;
		
		
	}

	@Override
	public void setValueAt(Object aValue, int row, int column) {
		// TODO Auto-generated method stub
		Medecin m = liste.get(row);
		modified.add(m);
		
		if (column == 0)
        {
            m.setMedecinId(aValue.toString());
        }
        else
         
        if (column == 1)
        {
        	m.setMedecinNom(aValue.toString());
        }
        else
         
        if (column == 2)
        {
        	m.setMedecinPrenom(aValue.toString());
        }
        else
         
        if (column == 3)
        {
        	m.setMedecinMail(aValue.toString());
        }
        else
         
        if (column == 4)
        {
        	m.setMedecinNumTel(aValue.toString());
        }
        else
            
        	if (column == 5)
            {
            	m.setMedecinAdresse(aValue.toString());
            }
	}

	@Override
	public Class getColumnClass(int column)
    {
        for (int row = 0; row < getRowCount(); row++)
        {
            Object o = getValueAt(row, column);

            if (o != null)
            {
                return o.getClass();
            }
        }

        return Object.class;
    }
	
	
	
	

}


import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.Window.Type;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpringLayout;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class ViewGestionStock extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JButton btnNewButton;
	private JButton btnNewButton_1;
	private JLabel lblNewLabel_1;
	private JLabel lblNewLabel_5;
	private JLabel lblNewLabel_6;
	private JLabel lblNewLabel_7;
	private JLabel lblNewLabel_8;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JLabel lblNewLabel_9;
	private JTextField textField_6;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewLaboratoireUpdate frame = new ViewLaboratoireUpdate();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ViewGestionStock() {
		setBounds(100, 100, 579, 771);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		panel.setForeground(Color.WHITE);
		panel.setBackground(Color.BLACK);
		
		JLabel lblNewLabel = new JLabel("G\u00E9rer le stock du m\u00E9dicament");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setForeground(Color.RED);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		panel.add(lblNewLabel_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setForeground(Color.WHITE);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
				.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 458, Short.MAX_VALUE)
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
					.addContainerGap())
		);
		
		JLabel lblNewLabel_2 = new JLabel("Identifiant du laboratoire ");
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_3 = new JLabel("Nom du m\u00E9dicament");
		lblNewLabel_3.setForeground(Color.WHITE);
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		JLabel lblNewLabel_4 = new JLabel("Description du m\u00E9dicament :");
		lblNewLabel_4.setForeground(Color.WHITE);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 13));
		
		textField = new JTextField();
		textField.setEditable(false);
		textField.setBackground(Color.WHITE);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setEditable(false);
		
		btnNewButton = new JButton("Modifier le stock");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		btnNewButton_1 = new JButton("Supprimer ce \r\nm\u00E9dicament pour ce laboratoire");
		btnNewButton_1.setForeground(new Color(255, 255, 255));
		btnNewButton_1.setBackground(Color.RED);
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		SpringLayout springLayout = new SpringLayout();
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton, 148, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton, -45, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, btnNewButton_1, 0, SpringLayout.WEST, btnNewButton);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton_1, -29, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, btnNewButton_1, -45, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_3, 0, SpringLayout.WEST, lblNewLabel_4);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_3, -361, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_4, 21, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, textField_1, 0, SpringLayout.WEST, textField);
		springLayout.putConstraint(SpringLayout.EAST, textField_1, 0, SpringLayout.EAST, textField);
		springLayout.putConstraint(SpringLayout.NORTH, textField, 38, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, textField, 210, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField, 427, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_2, 41, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_2, 19, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, lblNewLabel_2, 192, SpringLayout.WEST, panel_1);
		panel_1.setLayout(springLayout);
		panel_1.add(btnNewButton);
		panel_1.add(btnNewButton_1);
		panel_1.add(lblNewLabel_4);
		panel_1.add(lblNewLabel_3);
		panel_1.add(lblNewLabel_2);
		panel_1.add(textField_1);
		panel_1.add(textField);
		
		lblNewLabel_5 = new JLabel("Nom du laboratoire ");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_5, 28, SpringLayout.SOUTH, lblNewLabel_2);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_5, 0, SpringLayout.WEST, lblNewLabel_2);
		lblNewLabel_5.setForeground(Color.WHITE);
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel_1.add(lblNewLabel_5);
		
		lblNewLabel_6 = new JLabel("Adresse du laboratoire ");
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_6, 21, SpringLayout.WEST, panel_1);
		lblNewLabel_6.setForeground(Color.WHITE);
		lblNewLabel_6.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel_1.add(lblNewLabel_6);
		
		lblNewLabel_7 = new JLabel("Ville du laboratoire ");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_7, 31, SpringLayout.SOUTH, lblNewLabel_6);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_7, 21, SpringLayout.WEST, panel_1);
		lblNewLabel_7.setForeground(Color.WHITE);
		lblNewLabel_7.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel_1.add(lblNewLabel_7);
		
		lblNewLabel_8 = new JLabel("Stock du m\u00E9dicament :");
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_8, 21, SpringLayout.WEST, panel_1);
		lblNewLabel_8.setForeground(Color.WHITE);
		lblNewLabel_8.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel_1.add(lblNewLabel_8);
		
		scrollPane = new JScrollPane();
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton_1, 132, SpringLayout.SOUTH, scrollPane);
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_4, -19, SpringLayout.NORTH, scrollPane);
		springLayout.putConstraint(SpringLayout.NORTH, scrollPane, 349, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, scrollPane, -17, SpringLayout.NORTH, lblNewLabel_8);
		springLayout.putConstraint(SpringLayout.EAST, scrollPane, 392, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, scrollPane, 73, SpringLayout.WEST, panel_1);
		panel_1.add(scrollPane);
		
		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);
		
		textField_2 = new JTextField();
		textField_2.setEditable(false);
		springLayout.putConstraint(SpringLayout.NORTH, textField_2, 28, SpringLayout.SOUTH, textField);
		springLayout.putConstraint(SpringLayout.WEST, textField_2, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_2, 0, SpringLayout.EAST, textField_1);
		textField_2.setColumns(10);
		textField_2.setBackground(Color.WHITE);
		panel_1.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setEditable(false);
		springLayout.putConstraint(SpringLayout.NORTH, textField_3, 26, SpringLayout.SOUTH, textField_2);
		springLayout.putConstraint(SpringLayout.WEST, textField_3, 36, SpringLayout.EAST, lblNewLabel_6);
		springLayout.putConstraint(SpringLayout.EAST, textField_3, -128, SpringLayout.EAST, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_6, 0, SpringLayout.NORTH, textField_3);
		textField_3.setColumns(10);
		textField_3.setBackground(Color.WHITE);
		panel_1.add(textField_3);
		
		textField_4 = new JTextField();
		springLayout.putConstraint(SpringLayout.SOUTH, textField_4, -475, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.NORTH, textField_1, 26, SpringLayout.SOUTH, textField_4);
		textField_4.setEditable(false);
		springLayout.putConstraint(SpringLayout.WEST, textField_4, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_4, 0, SpringLayout.EAST, textField_1);
		textField_4.setColumns(10);
		textField_4.setBackground(Color.WHITE);
		panel_1.add(textField_4);
		
		textField_5 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, btnNewButton, 15, SpringLayout.SOUTH, textField_5);
		springLayout.putConstraint(SpringLayout.SOUTH, btnNewButton, 42, SpringLayout.SOUTH, textField_5);
		springLayout.putConstraint(SpringLayout.NORTH, textField_5, 522, SpringLayout.NORTH, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, textField_5, -104, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.SOUTH, lblNewLabel_8, -6, SpringLayout.NORTH, textField_5);
		springLayout.putConstraint(SpringLayout.WEST, textField_5, 125, SpringLayout.WEST, panel_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_5, 242, SpringLayout.WEST, panel_1);
		textField_5.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField_5.setColumns(10);
		panel_1.add(textField_5);
		
		lblNewLabel_9 = new JLabel("Identifiant du m\u00E9dicament");
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_9, 0, SpringLayout.NORTH, textField_1);
		springLayout.putConstraint(SpringLayout.WEST, lblNewLabel_9, 0, SpringLayout.WEST, lblNewLabel_4);
		lblNewLabel_9.setForeground(Color.WHITE);
		lblNewLabel_9.setFont(new Font("Tahoma", Font.BOLD, 13));
		panel_1.add(lblNewLabel_9);
		
		textField_6 = new JTextField();
		springLayout.putConstraint(SpringLayout.NORTH, lblNewLabel_3, 0, SpringLayout.NORTH, textField_6);
		springLayout.putConstraint(SpringLayout.NORTH, textField_6, 26, SpringLayout.SOUTH, textField_1);
		springLayout.putConstraint(SpringLayout.WEST, textField_6, 0, SpringLayout.WEST, textField_1);
		springLayout.putConstraint(SpringLayout.EAST, textField_6, 0, SpringLayout.EAST, textField_1);
		textField_6.setEditable(false);
		textField_6.setColumns(10);
		panel_1.add(textField_6);
		contentPane.setLayout(gl_contentPane);
	}
	public JTextField getTextField() {
		return textField;
	}
	public JTextField getTextField_1() {
		return textField_1;
	}
	
	
	public JButton getBtnNewButton() {
		return btnNewButton;
	}
	public JButton getBtnNewButton_1() {
		return btnNewButton_1;
	}
	
	public JLabel getLblNewLabel_1() {
		return lblNewLabel_1;
	}
	
public void Afficher(ViewLaboratoire vL) throws  SQLException {
		
		int i = vL.getTable_1().getSelectedRow();
		DefaultTableModel model =  (DefaultTableModel) vL.getTable_1().getModel();
		
		lblNewLabel_1.setText(model.getValueAt(i,0).toString());
		textField_6.setText(model.getValueAt(i,1).toString());
		textArea.setText(model.getValueAt(i,2).toString());

		textField_2.setText(vL.getTextArea().getText().toString());
		
		textField_3.setText(vL.getTextArea_1().getText().toString());
		
		textField_4.setText(vL.getTextArea_2().getText().toString());
		
		
		
		
		
		textField.setText(vL.getLblNewLabel_1().getText().toString());
		
		textField_1.setText(model.getValueAt(i,0).toString());
		
		
		
		
		
		
		
	}
	
	public JTextArea getTextArea() {
		return textArea;
	}
	public JTextField getTextField_6() {
		return textField_6;
	}
	public JTextField getTextField_5() {
		return textField_5;
	}
}
